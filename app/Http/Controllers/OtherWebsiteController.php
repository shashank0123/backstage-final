<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\ModuleController;
use Illuminate\Http\Request;
use App\Models\ProjectModule;
use Zip;
ini_set('max_execution_time', '30000');
class OtherWebsiteController extends Controller
{
    public function updatewebsite(Request $request)
    {
        $modulework = new ModuleController;
        // $modules
        $project_id = $request->project_id;
        $modules = ProjectModule::where('project_id', $project_id)->get();
        $response1['message'][] =  "There are total this modules" . count($modules);
        foreach ($modules as $key => $value) {
            $startModule = $key == 0 ? $value->id : $startModule;
            $endModule = $value->id;
        }
        $request->module_id = $startModule . '-' . $endModule;
        $responsewrite = $modulework->createView($request);
        $path = $this->zipData(public_path() . '/modules/' . $request->project_id, public_path() . '/modules/' . $request->project_id . 'modules.zip');
        $fileurl = url('modules/' . $request->project_id . 'modules.zip');
        // create folder for each modules using the code;
        // create all the folders for the modules;
        //step one get all the files to be copied
        //zip the file and store at a location
        // send the url to the server to download the file
        $response1 = "<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WebsiteTaskController extends Controller
{
    public function newtask(Request \$request)
    {
        //write the code to download the file.
        // write the code to extract the file.
        // write the code to read the task file and copy each file to correct place.
        // store the task in the db about what has been done
        \$zipfile = '$fileurl';
        \$path = public_path('/modules/');
        \File::makeDirectory(\$path, \$mode = 0777, true, true);
        \File::makeDirectory(\$path.'/files', \$mode = 0777, true, true);
        file_put_contents(\$path.'modules.zip', \$zipfile);
        echo \$filename = \$path.'module.zip';
        \$zip = new \ZipArchive;
        \$res = \$zip->open(\$filename);
        if (\$res === TRUE) {

        // Unzip path
        //\$path = \$path.' / files / ';

        // Extract file
        \$zip->extractTo(\$path);
        \$zip->close();

        echo 'Unzip!';
        } else {
        echo 'failed!';
        }


        // /add routes to the file
        $responsewrite;




    	echo 'now things will start working';
    }
}
";

        return $response1;
    }


    function zipData($source, $destination)
    {
        if (extension_loaded('zip')) {
            if (file_exists($source)) {
                $zip = new \ZipArchive();
                if ($zip->open($destination, \ZIPARCHIVE::CREATE)) {
                    // $source = realpath($source);
                    if (is_dir($source)) {
                        $iterator = new \RecursiveDirectoryIterator($source);
                        // skip dot files while iterating
                        $iterator->setFlags(\RecursiveDirectoryIterator::SKIP_DOTS);
                        $files = new \RecursiveIteratorIterator($iterator, \RecursiveIteratorIterator::SELF_FIRST);
                        foreach ($files as $file) {
                            $file = realpath($file);
                            if (is_dir($file)) {
                                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                            } else if (is_file($file)) {
                                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                            }
                        }
                    } else if (is_file($source)) {
                        $zip->addFromString(basename($source), file_get_contents($source));
                    }
                }
                return $zip->close();
            }
        }
        return false;
    }
}
