<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Theme;
use App\Models\ThemeElement;
use App\Http\Controllers\Controller;

class ThemeElementController extends Controller
{
    public function index()
    {
        $data = ThemeElement::paginate(20);
        return view('admin.ThemeElement.listThemeElement', compact('data'));
    }

    public function add()
    {
        $themes = Theme::all();
        return view('admin.ThemeElement.addThemeElement', compact('themes'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['theme_id'] = $data['theme_id'];
        $saveData['element_name'] = $data['element_name'];
        $saveData['code'] = $data['code'];
        $saveData['variables'] = $data['variables'];
        //if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ThemeElement = ThemeElement::create($saveData);

        // return response()->json(['success' => true, 'data' => $ThemeElement], 200);
        return redirect('/admin/themeelement')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = ThemeElement::where('id', $id)->first();
        return view('admin.ThemeElement.addThemeElement', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['theme_id'] = $data['theme_id'];
        $saveData['element_name'] = $data['element_name'];
        $saveData['code'] = $data['code'];
        $saveData['variables'] = $data['variables'];

        $row = ThemeElement::where('id', $id)->first();
        if ($row) {
            $ThemeElement = ThemeElement::where('id', $id)->update($saveData);
        }
        return view('admin.ThemeElement.listThemeElement')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = ThemeElement::where('id', $request->id)->delete();
        return view('admin.ThemeElement.listThemeElement');
    }
}
