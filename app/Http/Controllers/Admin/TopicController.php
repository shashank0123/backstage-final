<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Topic;
use App\Models\Subject;
use App\Http\Controllers\Controller;

class TopicController extends Controller
{
    public function index()
    {
        $data = Topic::paginate(20);
        return view('admin.Topic.listTopic', compact('data'));
    }

    public function add()
    {
        
$subjects = Subject::all();

        return view('admin.Topic.addTopic', compact('subjects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['subject_id'] = $data['subject_id'];
$saveData['topic_name'] = $data['topic_name'];
$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];


        $Topic = Topic::create($saveData);

        // return response()->json(['success' => true, 'data' => $Topic], 200);
        return redirect('/admin/topic')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Topic::where('id', $id)->first();$subjects = Subject::all();
return view('admin.Topic.addTopic', compact('row', 'subjects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('subject_id')) {
               $path = request()->file('subject_id')->store(
                   'file', 'public'
               );

               $data['subject_id'] = \Storage::disk('public')->url($path);

            }$saveData['subject_id'] = $data['subject_id'];

 if (request()->hasFile('topic_name')) {
               $path = request()->file('topic_name')->store(
                   'file', 'public'
               );

               $data['topic_name'] = \Storage::disk('public')->url($path);

            }$saveData['topic_name'] = $data['topic_name'];

 if (request()->hasFile('slug')) {
               $path = request()->file('slug')->store(
                   'file', 'public'
               );

               $data['slug'] = \Storage::disk('public')->url($path);

            }$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];

        $row = Topic::where('id', $id)->first();
        if ($row){
            $Topic = Topic::where('id', $id)->update($saveData);
        }
        return redirect('/admin/topic')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Topic::where('id', $request->id)->delete();
        return redirect('/admin/topic');

    }


    public function getData(){
        $data = Topic::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
