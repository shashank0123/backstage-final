<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\SmsNotification;
use App\Http\Controllers\Controller;

class SmsNotificationController extends Controller
{
    public function index()
    {
        $data = SmsNotification::paginate(20);
        return view('admin.SmsNotification.listSmsNotification', compact('data'));
    }

    public function add()
    {


        return view('admin.SmsNotification.addSmsNotification');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['notification_type'] = $data['notification_type'];
        $saveData['title'] = $data['title'];
        $saveData['message'] = $data['message'];
        $saveData['status'] = $data['status'];
        $saveData['exec_status'] = $data['exec_status'];


        $SmsNotification = SmsNotification::create($saveData);

        // return response()->json(['success' => true, 'data' => $SmsNotification], 200);
        return redirect('/admin/sms_notification')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = SmsNotification::where('id', $id)->first();
        return view('admin.SmsNotification.addSmsNotification', compact('row',));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['notification_type'] = $data['notification_type'];
        $saveData['title'] = $data['title'];
        $saveData['message'] = $data['message'];
        $saveData['status'] = $data['status'];
        $saveData['exec_status'] = $data['exec_status'];

        $row = SmsNotification::where('id', $id)->first();
        if ($row) {
            $SmsNotification = SmsNotification::where('id', $id)->update($saveData);
        }
        return redirect('/admin/sms_notification')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = SmsNotification::where('id', $request->id)->delete();
        return redirect('/admin/sms_notification');
    }


    public function getData()
    {
        $data = SmsNotification::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
