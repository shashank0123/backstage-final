<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProjectModule;

use File;

class ModuleController extends Controller
{
    public function createView(Request $request)
    {

        if (isset($request->module_id)) {
            $writeresponse = "";
            $items = explode('-', $request->module_id);
            $modules = ProjectModule::where('project_id', $request->project_id)->orWhere('project_id', 0)->get();
            $start_id = $items[0];
            $end_id = $items[1];
            $public_path = public_path() . '/modules/' . $request->project_id;
            File::makeDirectory($public_path, $mode = 0777, true, true);
            // dd($modules);
            foreach ($modules as $modulekey => $module) {
                // $module = ProjectModule::where('id', $modular)->first();
                $moduleName = $module->module_name;
                $fields = $module->db_fields;
                $field_type = $module->db_field_type;

                $copyfiles = 0;
                $modelname = str_replace(' ', '', $moduleName);
                $umodulename = str_replace(' ', '_', strtolower($moduleName));
                $db_fields = explode(',', $fields);
                $db_field_type = explode(',', $field_type);
                $db_field_type = $db_field_type;
                $path = $public_path . '/' . $modelname . '/';
                $makedir = File::makeDirectory($path, $mode = 0777, true, true);
                $modelPlacement = $this->modelCreate($path, $modelname, $db_fields);
                $migration_file_name = $this->migrationCreate($umodulename, $modelname, $db_fields, $path, $db_field_type);
                $controllerPlacement = $this->ControllerCreate($path, $modelname, $db_fields, $umodulename, $db_field_type);
                $filename = $this->viewCreate($path, $modelname, $umodulename, $db_fields, $moduleName, $db_field_type);

                $routeToBeAdded = $this->routeCreate($path, $umodulename, $modelname);
                $sidebar = $this->sidebarCreate($path, $moduleName, $umodulename);
                if ($copyfiles == 1) {
                    $copyFiles = $this->copyFilesToCurrentProject($routeToBeAdded, $path, $modelname, $migration_file_name, $filename);
                } else {
                    $writeresponse .= $this->writeFilesToFile($routeToBeAdded, $path, $modelname, $migration_file_name, $filename);
                }
            }
            return $writeresponse;
        } else
            return 0;
    }

    public function modelCreate($path, $modelname, $db_fields)
    {
        $modelFileContent = "<?php\n\nnamespace App\Models;\nuse Illuminate\Database\Eloquent\Model;\n\nclass " . $modelname . " extends Model\n{\nprotected \$fillable = " . json_encode($db_fields) . ";\n}";
        file_put_contents($path . $modelname . ".php", $modelFileContent);
    }

    public function migrationCreate($umodulename, $modelname, $db_fields, $path, $db_field_type)
    {
        $migration_file_name = date('Y_m_d') . '_' . rand(100000, 100000) . '_create_' . $umodulename . "_table.php";
        $migrationFileContent = "<?php\n\nuse Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create" . $modelname . "Table extends Migration\n
{
    public function up()
    {
        Schema::create('" . \Str::plural($umodulename) . "', function (Blueprint \$table) {
            \$table->id();\n";

        foreach ($db_fields as $key => $value) {
            if ($db_field_type[$key] == 'tinymce')
                $migrationFileContent .= "\$table->text('" . $value . "')->nullable();\n";
            else
                $migrationFileContent .= "\$table->string('" . $value . "')->nullable();\n";
        }


        $migrationFileContent .= "\$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('" . \Str::plural($umodulename) . "');
    }
}
";
        file_put_contents($path . $migration_file_name, $migrationFileContent);
        return $migration_file_name;
    }


    public function controllerCreate($path, $modelname, $db_fields, $umodulename, $db_field_type)
    {
        $controller_file_name = $modelname . "Controller.php";
        $addfilename = 'add' . $modelname;
        $listfilename = 'list' . $modelname;

        $controllerFileContent = "<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\\$modelname;\n";

        foreach ($db_field_type as $key => $value) {
            if (strpos($value, 'ropdown') > 0) {
                $value2 = explode('-', $value);
                if ($modelname != $value2[1])
                $controllerFileContent .= "use App\Models\\" . $value2[1] . ";\n";
            }
        }

        $controllerFileContent .= "use App\Http\Controllers\Controller;

class " . $modelname . "Controller extends Controller
{
    public function index()
    {
        \$data = $modelname::paginate(20);
        return view('admin." . $modelname . "." . $listfilename .
            "', compact('data'));
    }

    public function add()
    {
        \n";
        $count = 0;
        $allvariable = '';
        $importlines = '';
        foreach ($db_field_type as $key => $value) {
            if (strpos($value, 'ropdown') > 0) {
                
                $value2 = explode('-', $value);
                $newname = strtolower($value2[1]) . 's';
                if ($allvariable != '') {
                    $allvariable .= ',';
                }
                $allvariable .= "'" . $newname . "'";
                $importlines .= '$' . $newname . ' = ' . $value2[1] . "::all();\n";
            }
        }
        
        $controllerFileContent .= $importlines;
        $controllerFileContent .= "
        return view('admin." . $modelname . "." . $addfilename . "'" .( $allvariable != "" ? ', compact(' : '') . $allvariable  . ($allvariable != "" ? ')' : ''). ");
    }

    public function store(Request \$request)
    {
        //\$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if (\$validator->fails()) {
        //    return response()->json(['error' => \$validator->messages()->first()], 500);
        //}

        \$data = request()->all();
        \$saveData = [];\n";
        foreach ($db_fields as $key => $value) {
            if ($db_field_type[$key] == 'file') {
                $controllerFileContent .= "\n if (request()->hasFile('$value')) {
               \$path = request()->file('$value')->store(
                   'file', 'public'
               );

               \$data['$value'] = \Storage::disk('public')->url(\$path);

            }";
            }
            else
            $controllerFileContent .= "\$saveData['$value'] = \$data['$value'];\n";
        }

        $controllerFileContent .= "

        \$$modelname = $modelname::create(\$saveData);

        // return response()->json(['success' => true, 'data' => \$$modelname], 200);
        return redirect('/admin/" . $umodulename . "')->with('successMsg','Data has been saved.');
    }

    public function edit(\$id)
    {
        \$row = $modelname::where('id', \$id)->first();";
        $controllerFileContent .= $importlines;

        $controllerFileContent .= "return view('admin." . $modelname . "." . $addfilename .
            "', compact('row'".( $allvariable != '' ? ',':'')." $allvariable));
    }

    public function update(\$id, Request \$request)
    {
        \$data = request()->all();
        \$saveData = [];\n";
        foreach ($db_fields as $key => $value) {
            if ($db_field_type[$key] == 'file') {
                $controllerFileContent .= "\n if (request()->hasFile('$value')) {
               \$path = request()->file('$value')->store(
                   'file', 'public'
               );

               \$saveData['$value'] = \Storage::disk('public')->url(\$path);

            }";
            }
            else
            $controllerFileContent .= "\$saveData['$value'] = \$data['$value'];\n";
        }

        $controllerFileContent .= "
        \$row = $modelname::where('id', \$id)->first();
        if (\$row){
            \$$modelname = $modelname::where('id', \$id)->update(\$saveData);
        }
        return redirect('/admin/$umodulename')->with('successMsg','Data has been updated.');

    }

    public function delete(Request \$request)
    {
        \$delete = $modelname::where('id', \$request->id)->delete();
        return redirect('/admin/$umodulename');

    }


    public function getData(){
        \$data = $modelname::all();
        return response()->json(['data' => \$data, 'success' => true, 'message' => 'data retrieved']);
    }
}
";


        $makedir = File::makeDirectory($path . '/' . $modelname, $mode = 0777, true, true);
        file_put_contents($path . $controller_file_name, $controllerFileContent);
    }


    public function viewCreate($path, $modelname, $umodulename, $db_fields, $moduleName, $db_field_type)
    {
        $addfilename = 'add' . $modelname;
        $listfilename = 'list' . $modelname;
        $viewpath = $path . '/' . $modelname . '/';
        $addFileContent = "@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='' method='post' enctype='multipart/form-data'>
@csrf
";




        foreach ($db_fields as $key => $value) {
            # code...
            if ($db_field_type[$key] == 'text')
                $addFileContent .= "<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>" . ucwords(str_replace('_', ' ', $value)) . "</label>
                <input class='form-control btn-square' type='text' value='@if (isset(\$row->$value)){{\$row->$value}} @endif' name='$value' placeholder='Enter " . ucwords(str_replace('_', ' ', $value)) . "'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>\n";
            else if (strpos($db_field_type[$key], 'ropdown') > 0) {
                $value3 = explode('-', $db_field_type[$key]);
                $newname = strtolower($value3[1]) . 's';
                $addFileContent .= "<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>$value3[1]</label>
            <select class='form-control btn-square' name='$value' id='$value'>
                    <option value=''>Select</option>
                @foreach ($" . $newname . " as \$item)
                    <option @if (isset(\$row->$value) && \$row->$value == \$item->id){{'selected'}} @endif value='{{\$item->id}}'>{{\$item->$value3[2]}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div>";
            }

            else if ($db_field_type[$key] == 'file') {
                $addFileContent .= "<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='$value'>$value</label>
                    @if (isset(\$row->$value) && \$row->$value!='')<img width='200px' height='200px' src='{{\$row->$value}}'> @endif

                    <div class='col-lg-12'>
                        <input id='$value' name='$value' class='input-file' type='file'>
                    </div>
                    </div>";
            }
            else if ($db_field_type[$key] == 'tinymce'){
                $addFileContent .= "<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='$value'>$value</label>
                    <div class='col-lg-12'>
                        <textarea name='$value' class='tinyMCE'>@if (isset(\$row->$value)){{\$row->$value}} @endif</textarea>
                    </div>
                    </div>";
            }
        }

        $addFileContent .= "</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/$umodulename' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection";
        $listFileContent = "@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/$umodulename/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>$moduleName </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr>";
        foreach ($db_fields as  $value) {
            # code...
            $listFileContent .= "<th>" . ucwords(str_replace('_', ' ', $value)) . "</th>";
        }

        $listFileContent .= "<th>Action</th></tr>
                        </thead>
                        <tbody>";

        $listFileContent .= "
                        @foreach (\$data as  \$value) {
                        <tr>";
        foreach ($db_fields as  $value2) {

            $listFileContent .= "<td>{{\$value->$value2}}</td>";
        }
        $listFileContent .= "<td><a href='/admin/$umodulename/edit/{{\$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/$umodulename/delete/{{\$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td>";
        $listFileContent .= "</tr>@endforeach";

        $listFileContent .= "</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection";

        file_put_contents($viewpath . $addfilename . '.blade.php', $addFileContent);
        file_put_contents($viewpath . $listfilename . '.blade.php', $listFileContent);
        return [$listfilename, $addfilename];
    }


    public function routeCreate($path, $umodulename, $modelname)
    {
        $routeToBeAdded = "Route::get('/" . $umodulename . "','Admin\\" . $modelname . "Controller@index');
    Route::get('" . $umodulename . "/add','Admin\\" . $modelname . "Controller@add');
    Route::post('" . $umodulename . "/add','Admin\\" . $modelname . "Controller@store');
    Route::get('" . $umodulename . "/edit/{id}','Admin\\" . $modelname . "Controller@edit');
    Route::post('" . $umodulename . "/edit/{id}','Admin\\" . $modelname . "Controller@update');
    Route::get('" . $umodulename . "/delete/{id}','Admin\\" . $modelname . "Controller@delete');";
        file_put_contents($path . 'routes.php', $routeToBeAdded);
        return $routeToBeAdded;
    }

    public function sidebarCreate($path, $moduleName, $umodulename)
    {
        $sideBarContent = "<li class='dropdown'><a class='nav-link menu-title' href='#'><i data-feather='folder-plus'></i><span>$moduleName</span></a>
                    <ul class='nav-submenu menu-content'>
                      <li><a href='/admin/$umodulename/add'>Create $moduleName</a></li>
                      <li><a href='/admin/$umodulename'>$moduleName</a></li>
                    </ul>
                  </li>";
        file_put_contents($path . 'sidebar.php', $sideBarContent);
    }


    public function copyFilesToCurrentProject($routeToBeAdded, $path, $modelname, $migration_file_name, $filename)
    {
        $routefilecontent = file_get_contents(base_path() . '/routes/admin.php');
        $position = strpos($routefilecontent, $routeToBeAdded);
        if (!$position > 0) {
            $routefilecontent = str_replace("//{Route to be added here from automation}", $routeToBeAdded . "\n//{Route to be added here from automation}", $routefilecontent);
            file_put_contents(base_path() . '/routes/admin.php', $routefilecontent);
        }
        copy($path . $modelname . '.php', base_path() . '/app/Models/' . $modelname . '.php');
        copy($path . $modelname . 'Controller.php', base_path() . '/app/Http/Controllers/Admin/' . $modelname . 'Controller.php');
        copy($path . $migration_file_name, base_path() . '/database/migrations//' . $migration_file_name);
        $viewPath = base_path() . '/resources/views/admin/' . $modelname . '/';
        $viewpath = $path . '/' . $modelname . '/';
        if (!file_exists($viewPath))
            $makedir = File::makeDirectory($viewPath);
        copy($viewpath . $filename[1] . '.blade.php', $viewPath . $filename[1] . '.blade.php');
        copy($viewpath . $filename[0] . '.blade.php', $viewPath . $filename[0] . '.blade.php');
    }

    public function writeFilesToFile($routeToBeAdded, $path, $modelname, $migration_file_name, $filename)
    {

        $response2 = "\$routefilecontent = file_get_contents(base_path() . '/routes/admin.php');\n";
        $response2 .= "\$routeToBeAdded = \"$routeToBeAdded\";\n";
        $response2 .= "\$path = public_path().'/modules/'.'$modelname'.'/';\n";

        $response2 .= "\$position = strpos(\$routefilecontent, \$routeToBeAdded);\n";
        $response2 .= "if (!\$position > 0) {\n";
        $response2 .= "\$routefilecontent = str_replace(\"//{Route to be added here from automation}\", \"\$routeToBeAdded  \\n//{Route to be added here from automation}\", \$routefilecontent);";
        $response2 .= 'file_put_contents(base_path() . "/routes/admin.php", "$routefilecontent");';
        $response2 .= "}";
        $response2 .= "copy(\$path . '$modelname' . '.php', base_path() . '/app/Models/' . '$modelname' . '.php');\n";
        $response2 .= "copy(\$path . '$modelname' . 'Controller.php', base_path() . '/app/Http/Controllers/Admin/' . '$modelname' . 'Controller.php');\n";
        $response2 .= "copy(\$path . '$migration_file_name', base_path() . '/database/migrations//' . '$migration_file_name');\n";
        $response2 .= "\$viewPath = base_path() . '/resources/views/admin/' . '$modelname' . '/';\n";
        $response2 .= "\$viewpath = \$path . '/' . '$modelname' . '/';
        if (!file_exists(\$viewPath))
            \$makedir = \\File::makeDirectory(\$viewPath);\n";
        $response2 .= "copy(\$viewpath . '$filename[1]' . '.blade.php', \$viewPath . '$filename[1]' . '.blade.php');\n";
        $response2 .= "copy(\$viewpath . '$filename[0]' . '.blade.php', \$viewPath . '$filename[0]' . '.blade.php');\n";

        return $response2;
    }
}
