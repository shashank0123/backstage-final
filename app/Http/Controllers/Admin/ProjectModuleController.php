<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectModule;
use App\Models\Project;
use App\Models\Theme;
use App\Http\Controllers\Controller;
use File;

class ProjectModuleController extends Controller
{
    public function index()
    {
        $data = ProjectModule::paginate(20);
        return view('admin.ProjectModule.listProjectModule', compact('data'));
    }

    public function add()
    {
        $projects = Project::all();
        $themes = Theme::all();
        return view('admin.ProjectModule.addProjectModule', compact('projects', 'themes'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['project_id'] = $data['project_id'];
        $saveData['module_name'] = $data['module_name'];
        $saveData['model_name'] = $data['module_name'];
        $saveData['db_fields'] = $data['db_fields'];
        $saveData['db_field_type'] = $data['db_field_type'];
        $saveData['theme_id'] = $data['theme_id'];
        $saveData['feature_for'] = $data['feature_for'];
        $saveData['controller_name'] = $data['controller_name'];
        $saveData['view_folder_name'] = $data['view_folder_name'];
        $saveData['options_to_add'] = $data['options_to_add'];
        $saveData['status'] = $data['status'];
        $saveData['is_template'] = $data['is_template'];
        $saveData['is_processed'] = $data['is_processed'];
        //if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProjectModule = ProjectModule::create($saveData);

        return redirect('/admin/project_module')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectModule::where('id', $id)->first();
        return view('admin.ProjectModule.addProjectModule', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['project_id'] = $data['project_id'];
        $saveData['module_name'] = $data['module_name'];
        $saveData['model_name'] = $data['model_name'];
        $saveData['db_fields'] = $data['db_fields'];
        $saveData['db_fields_type'] = $data['db_fields_type'];
        $saveData['theme_id'] = $data['theme_id'];
        $saveData['feature_for'] = $data['feature_for'];
        $saveData['controller_name'] = $data['controller_name'];
        $saveData['view_folder_name'] = $data['view_folder_name'];
        $saveData['options_to_add'] = $data['options_to_add'];
        $saveData['status'] = $data['status'];
        $saveData['is_template'] = $data['is_template'];
        $saveData['is_processed'] = $data['is_processed'];

        $row = ProjectModule::where('id', $id)->first();

        if ($row) {
            $ProjectModule = ProjectModule::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectModule.listProjectModule')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = ProjectModule::where('id', $request->id)->delete();
        return view('admin.ProjectModule.listProjectModule');
    }


    public function exportModule($id)
    {
        $moduleName = "Slider";
        $fields = "image,slider_text,start_date,end_date,status";
        $copyfiles = 0;
        $modelname = str_replace(' ', '', $moduleName);
        $umodulename = str_replace(' ', '_', strtolower($moduleName));
        $db_fields = explode(',', $fields);
        $db_fields = $db_fields;
        $path = public_path() . '/modules/' . $modelname . '/';
        $makedir = File::makeDirectory(
            $path,
            $mode = 0777,
            true,
            true
        );
        echo "Hi! You are here to witness something awesome";
        echo "<br>we will create a complete module and then move it to its proper place";
        echo "<br>base location for all the magic will be in public folder " . public_path();
        $modelFileContent = "<?php\n\nnamespace App\Models;\nuse Illuminate\Database\Eloquent\Model;\n\nclass " . $modelname . " extends Model\n{\nprotected \$fillable = " . json_encode($db_fields) . ";\n}";
        file_put_contents($path . $modelname . ".php", $modelFileContent);
        echo "<br> we will put the content of the model first that is ";
        echo "<br> model creation done";
        echo "<br> now we will create the migration";
        $migration_file_name = date('Y_m_d') . '_' . rand(100000, 100000) . '_create_' . $umodulename . "_table.php";
        $migrationFileContent = "<?php\n\nuse Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Create" . $modelname . "Table extends Migration\n
{
    public function up()
    {
        Schema::create('" . $umodulename . "s', function (Blueprint \$table) {
            \$table->id();\n";

        foreach ($db_fields as $key => $value) {
            $migrationFileContent .= "\$table->string('" . $value . "')->nullable();\n";
        }


        $migrationFileContent .= "\$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('" . $umodulename . "s');
    }
}
";
        file_put_contents($path . $migration_file_name, $migrationFileContent);
        echo "<br> migration also written";

        echo "<br> now the controller part";
        $controller_file_name = $modelname . "Controller.php";
        $addfilename = 'add' . $modelname;
        $listfilename = 'list' . $modelname;
        $controllerFileContent = "<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\\$modelname;
use App\Http\Controllers\Controller;

class " . $modelname . "Controller extends Controller
{
    public function index()
    {
        \$data = $modelname::paginate(20);
        return view('admin." . $modelname . "." . $listfilename . "', compact('data'));
    }

    public function add()
    {
        return view('admin." . $modelname . "." . $addfilename . "');
    }

    public function store(Request \$request)
    {
        //\$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if (\$validator->fails()) {
        //    return response()->json(['error' => \$validator->messages()->first()], 500);
        //}

        \$data = request()->all();
        \$saveData = [];\n";
        foreach ($db_fields as $value) {
            $controllerFileContent .= "\$saveData['$value'] = \$data['$value'];\n";
        }

        $controllerFileContent .= "//if (request()->hasFile('category_image')) {
        //    \$path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    \$data['category_image'] = \Storage::disk('public')->url(\$path);

        //}

        \$$modelname = $modelname::create(\$saveData);

        // return response()->json(['success' => true, 'data' => \$$modelname], 200);
        return redirect('/admin/" . $umodulename . "')->with('successMsg','Data has been saved.');
    }

    public function edit(\$id)
    {
        \$row = $modelname::where('id', \$id)->first();
        return view('admin." . $modelname . "." . $addfilename .
            "', compact('row'));
    }

    public function update(\$id, Request \$request)
    {
        \$data = request()->all();
        \$saveData = [];\n";
        foreach ($db_fields as $value) {
            $controllerFileContent .= "\$saveData['$value'] = \$data['$value'];\n";
        }

        $controllerFileContent .= "
        \$row = $modelname::where('id', \$id)->first();
        if (\$row){
            \$$modelname = $modelname::where('id', \$id)->update(\$saveData);
        }
        return view('admin." . $modelname . "." . $listfilename . "')->with('successMsg','Data has been updated.');

    }

    public function delete(Request \$request)
    {
        \$delete = $modelname::where('id', \$request->id)->delete();
        return view('admin." . $modelname . "." . $listfilename . "');
    }
}
";


        $makedir = File::makeDirectory($path . '/' . $modelname, $mode = 0777, true, true);
        file_put_contents($path . $controller_file_name, $controllerFileContent);
        echo "<br> controller also written";
        echo "<br> now we have to work on view. Controller part is left a littlebit. but we will be back in a moment";
        $viewpath = $path . '/' . $modelname . '/';
        $addFileContent = "@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/$umodulename/add' method='post'>@csrf";

        foreach ($db_fields as $value) {
            # code...
            $addFileContent .= "<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>" . ucwords(str_replace('_', ' ', $value)) . "</label>
                <input class='form-control btn-square' type='text' name='$value' placeholder='Enter " . ucwords(str_replace('_', ' ', $value)) . "'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>";
        }

        $addFileContent .= "</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/$umodulename' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection";
        $listFileContent = "@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/$umodulename/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>$moduleName </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr>";
        foreach ($db_fields as  $value) {
            # code...
            $listFileContent .= "<th>" . ucwords(str_replace('_', ' ', $value)) . "</th>";
        }

        $listFileContent .= "<th>Action</th></tr>
                        </thead>
                        <tbody>";

        $listFileContent .= "
                        @foreach (\$data as  \$value)
                        <tr>";
        foreach ($db_fields as  $value2) {

            $listFileContent .= "<td>{{\$value->$value2}}</td>";
        }
        $listFileContent .= "<td><a href='/admin/$umodulename/edit/\$value->id' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/$umodulename/delete/\$value->id' class='btn btn-danger' data-original-title='' title=''>delete</a></td>";
        $listFileContent .= "</tr>@endforeach";

        $listFileContent .= "</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection";

        file_put_contents($viewpath . $addfilename . '.blade.php', $addFileContent);
        file_put_contents($viewpath . $listfilename . '.blade.php', $listFileContent);


        $routeToBeAdded = "Route::get('/" . $umodulename . "','Admin\\" . $modelname . "Controller@index');
    Route::get('" . $umodulename . "/add','Admin\\" . $modelname . "Controller@add');
    Route::post('" . $umodulename . "/add','Admin\\" . $modelname . "Controller@store');
    Route::get('" . $umodulename . "/edit/{id}','Admin\\" . $modelname . "Controller@edit');
    Route::post('" . $umodulename . "/edit/{id}','Admin\\" . $modelname . "Controller@update');
    Route::post('" . $umodulename . "/delete/{id}','Admin\\" . $modelname . "Controller@delete');";
        file_put_contents($path . 'routes.php', "<?php \n\nuse Illuminate\Support\Facades\Route;\n\n" . $routeToBeAdded . "\n?>");
        echo "<br>";
        $routefilecontent = file_get_contents(base_path() . '/routes/admin.php');

        echo $position = strpos($routefilecontent, $routeToBeAdded);
        echo "<br>";
        if (!$position > 0) {
            $routefilecontent = str_replace(
                "//{Route to be added here from automation}",
                $routeToBeAdded . "\n//{Route to be added here from automation}",
                $routefilecontent
            );
            file_put_contents(base_path() . '/routes/admin.php', $routefilecontent);
        }
        echo "<br> route is also added now. Only view making is left";

        if ($copyfiles == 1) {
            // copy($path . $modelname . '.php', base_path() . '/app/Models/' . $modelname . '.php');
            // copy($path . $modelname . 'Controller.php', base_path() . '/app/Http/Controllers/Admin/' . $modelname . 'Controller.php');
            // copy($path . $migration_file_name, base_path() . '/database/migrations//' . $migration_file_name);
            // $viewPath = base_path() . '/resources/views/admin/' . $modelname . '/';
            // if (!file_exists($viewPath))
            //     $makedir = File::makeDirectory($viewPath);
            // copy($viewpath . $addfilename . '.blade.php', $viewPath . $addfilename . '.blade.php');
            // copy($viewpath . $listfilename . '.blade.php', $viewPath . $listfilename . '.blade.php');
        }
    }
}
