<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\TeacherSubject;
use App\Models\Teacher;
use App\Models\Subject;
use App\Http\Controllers\Controller;

class TeacherSubjectController extends Controller
{
    public function index()
    {
        $data = TeacherSubject::paginate(20);
        return view('admin.TeacherSubject.listTeacherSubject', compact('data'));
    }

    public function add()
    {
        
$teachers = Teacher::all();
$subjects = Subject::all();

        return view('admin.TeacherSubject.addTeacherSubject', compact('teachers','subjects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['teacher_id'] = $data['teacher_id'];
$saveData['subject_id'] = $data['subject_id'];


        $TeacherSubject = TeacherSubject::create($saveData);

        // return response()->json(['success' => true, 'data' => $TeacherSubject], 200);
        return redirect('/admin/teachersubject')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = TeacherSubject::where('id', $id)->first();$teachers = Teacher::all();
$subjects = Subject::all();
return view('admin.TeacherSubject.addTeacherSubject', compact('row', 'teachers','subjects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['teacher_id'] = $data['teacher_id'];
$saveData['subject_id'] = $data['subject_id'];

        $row = TeacherSubject::where('id', $id)->first();
        if ($row){
            $TeacherSubject = TeacherSubject::where('id', $id)->update($saveData);
        }
        return redirect('/admin/teachersubject')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = TeacherSubject::where('id', $request->id)->delete();
        return redirect('/admin/teachersubject');

    }


    public function getData(){
        $data = TeacherSubject::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
