<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectLanguage;
use App\Http\Controllers\Controller;

class ProjectLanguageController extends Controller
{
    public function index()
    {
        $data = ProjectLanguage::paginate(20);
        return view('admin.ProjectLanguage.listProjectLanguage', compact('data'));
    }

    public function add()
    {
        return view('admin.ProjectLanguage.addProjectLanguage');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['language_id'] = $data['language_id'];
$saveData['version'] = $data['version'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProjectLanguage = ProjectLanguage::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectLanguage], 200);
        return redirect('/admin/project_language')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectLanguage::where('id', $id)->first();
        return view('admin.ProjectLanguage.addProjectLanguage', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['language_id'] = $data['language_id'];
$saveData['version'] = $data['version'];

        $row = ProjectLanguage::where('id', $id)->first();
        if ($row){
            $ProjectLanguage = ProjectLanguage::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectLanguage.listProjectLanguage')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectLanguage::where('id', $request->id)->delete();
        return view('admin.ProjectLanguage.listProjectLanguage');
    }
}
