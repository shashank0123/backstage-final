<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Invoice;
use App\Http\Controllers\Controller;

class InvoiceController extends Controller
{
    public function index()
    {
        $data = Invoice::paginate(20);
        return view('admin.Invoice.listInvoice', compact('data'));
    }

    public function add()
    {
        return view('admin.Invoice.addInvoice');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['invoice_no'] = $data['invoice_no'];
$saveData['company_id'] = $data['company_id'];
$saveData['invoice_template_id'] = $data['invoice_template_id'];
$saveData['invoice_for'] = $data['invoice_for'];
$saveData['address'] = $data['address'];
$saveData['amount'] = $data['amount'];
$saveData['due_date'] = $data['due_date'];
$saveData['extra_details'] = $data['extra_details'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $Invoice = Invoice::create($saveData);

        // return response()->json(['success' => true, 'data' => $Invoice], 200);
        return redirect('/admin/invoice')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Invoice::where('id', $id)->first();
        return view('admin.Invoice.addInvoice', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['invoice_no'] = $data['invoice_no'];
$saveData['company_id'] = $data['company_id'];
$saveData['invoice_template_id'] = $data['invoice_template_id'];
$saveData['invoice_for'] = $data['invoice_for'];
$saveData['address'] = $data['address'];
$saveData['amount'] = $data['amount'];
$saveData['due_date'] = $data['due_date'];
$saveData['extra_details'] = $data['extra_details'];

        $row = Invoice::where('id', $id)->first();
        if ($row){
            $Invoice = Invoice::where('id', $id)->update($saveData);
        }
        return view('admin.Invoice.listInvoice')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Invoice::where('id', $request->id)->delete();
        return view('admin.Invoice.listInvoice');
    }
}
