<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Client;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    public function index()
    {
        $data = Client::paginate(20);
        return view('admin.Client.listClient', compact('data'));
    }

    public function add()
    {
        return view('admin.Client.addClient');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['address'] = $data['address'];
$saveData['company'] = $data['company'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $Client = Client::create($saveData);

        // return response()->json(['success' => true, 'data' => $Client], 200);
        return redirect('/admin/client')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Client::where('id', $id)->first();
        return view('admin.Client.addClient', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['email'] = $data['email'];
$saveData['phone'] = $data['phone'];
$saveData['address'] = $data['address'];
$saveData['company'] = $data['company'];

        $row = Client::where('id', $id)->first();
        if ($row){
            $Client = Client::where('id', $id)->update($saveData);
        }
        return view('admin.Client.listClient')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Client::where('id', $request->id)->delete();
        return view('admin.Client.listClient');
    }
}
