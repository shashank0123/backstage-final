<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\StudentClass;
use App\Http\Controllers\Controller;

class StudentClassController extends Controller
{
    public function index()
    {
        $data = StudentClass::paginate(20);
        return view('admin.StudentClass.listStudentClass', compact('data'));
    }

    public function add()
    {
        

        return view('admin.StudentClass.addStudentClass', compact());
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['class_name'] = $data['class_name'];
$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];


        $StudentClass = StudentClass::create($saveData);

        // return response()->json(['success' => true, 'data' => $StudentClass], 200);
        return redirect('/admin/studentclass')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = StudentClass::where('id', $id)->first();return view('admin.StudentClass.addStudentClass', compact('row', ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('class_name')) {
               $path = request()->file('class_name')->store(
                   'file', 'public'
               );

               $data['class_name'] = \Storage::disk('public')->url($path);

            }$saveData['class_name'] = $data['class_name'];

 if (request()->hasFile('slug')) {
               $path = request()->file('slug')->store(
                   'file', 'public'
               );

               $data['slug'] = \Storage::disk('public')->url($path);

            }$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];

        $row = StudentClass::where('id', $id)->first();
        if ($row){
            $StudentClass = StudentClass::where('id', $id)->update($saveData);
        }
        return redirect('/admin/studentclass')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = StudentClass::where('id', $request->id)->delete();
        return redirect('/admin/studentclass');

    }


    public function getData(){
        $data = StudentClass::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
