<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Theme;
use App\Http\Controllers\Controller;

class ThemeController extends Controller
{
    public function index()
    {
        $data = Theme::paginate(20);
        return view('admin.Theme.listTheme', compact('data'));
    }

    public function add()
    {
        return view('admin.Theme.addTheme');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['type'] = $data['type'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $Theme = Theme::create($saveData);

        // return response()->json(['success' => true, 'data' => $Theme], 200);
        return redirect('/admin/theme')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Theme::where('id', $id)->first();
        return view('admin.Theme.addTheme', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['name'] = $data['name'];
$saveData['type'] = $data['type'];

        $row = Theme::where('id', $id)->first();
        if ($row){
            $Theme = Theme::where('id', $id)->update($saveData);
        }
        return view('admin.Theme.listTheme')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Theme::where('id', $request->id)->delete();
        return view('admin.Theme.listTheme');
    }
}
