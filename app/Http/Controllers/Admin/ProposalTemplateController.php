<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProposalTemplate;
use App\Http\Controllers\Controller;

class ProposalTemplateController extends Controller
{
    public function index()
    {
        $data = ProposalTemplate::paginate(20);
        return view('admin.ProposalTemplate.listProposalTemplate', compact('data'));
    }

    public function add()
    {
        return view('admin.ProposalTemplate.addProposalTemplate');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['template_name'] = $data['template_name'];
$saveData['template_for'] = $data['template_for'];
$saveData['template_tag'] = $data['template_tag'];
$saveData['template_file'] = $data['template_file'];
$saveData['created_by'] = $data['created_by'];
$saveData['status'] = $data['status'];
$saveData['company_id'] = $data['company_id'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProposalTemplate = ProposalTemplate::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProposalTemplate], 200);
        return redirect('/admin/proposal_template')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProposalTemplate::where('id', $id)->first();
        return view('admin.ProposalTemplate.addProposalTemplate', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['template_name'] = $data['template_name'];
$saveData['template_for'] = $data['template_for'];
$saveData['template_tag'] = $data['template_tag'];
$saveData['template_file'] = $data['template_file'];
$saveData['created_by'] = $data['created_by'];
$saveData['status'] = $data['status'];
$saveData['company_id'] = $data['company_id'];

        $row = ProposalTemplate::where('id', $id)->first();
        if ($row){
            $ProposalTemplate = ProposalTemplate::where('id', $id)->update($saveData);
        }
        return view('admin.ProposalTemplate.listProposalTemplate')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProposalTemplate::where('id', $request->id)->delete();
        return view('admin.ProposalTemplate.listProposalTemplate');
    }
}
