<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectDependency;
use App\Http\Controllers\Controller;

class ProjectDependencyController extends Controller
{
    public function index()
    {
        $data = ProjectDependency::paginate(20);
        return view('admin.ProjectDependency.listProjectDependency', compact('data'));
    }

    public function add()
    {
        return view('admin.ProjectDependency.addProjectDependency');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['dependency_id'] = $data['dependency_id'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProjectDependency = ProjectDependency::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectDependency], 200);
        return redirect('/admin/project_dependency')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectDependency::where('id', $id)->first();
        return view('admin.ProjectDependency.addProjectDependency', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['dependency_id'] = $data['dependency_id'];

        $row = ProjectDependency::where('id', $id)->first();
        if ($row){
            $ProjectDependency = ProjectDependency::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectDependency.listProjectDependency')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectDependency::where('id', $request->id)->delete();
        return view('admin.ProjectDependency.listProjectDependency');
    }
}
