<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Http\Controllers\Controller;

class ProjectController extends Controller
{
    public function index()
    {
        $data = Project::paginate(20);
        return view('admin.Project.listProject', compact('data'));
    }

    public function add()
    {
        return view('admin.Project.addProject');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['project_name'] = $data['project_name'];
        $saveData['priority'] = $data['priority'];
        $saveData['project_url'] = $data['project_url'];
        $saveData['start_date'] = $data['start_date'];
        $saveData['end_date'] = $data['end_date'];
        $saveData['modules'] = $data['modules'];
        $saveData['type'] = $data['type'];
        $saveData['url'] = $data['url'];
        $saveData['git'] = $data['git'];
        $saveData['folder'] = $data['folder'];
        $saveData['about'] = $data['about'];
        $saveData['total_time'] = $data['total_time'];
        $saveData['total_amount'] = $data['total_amount'];
        $saveData['client_id'] = $data['client_id'];
        $saveData['language_id'] = $data['language_id'];
        $saveData['version'] = $data['version'];
        //if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $Project = Project::create($saveData);

        // return response()->json(['success' => true, 'data' => $Project], 200);
        return redirect('/admin/project')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $project = Project::where('id', $id)->first();
        return view('admin.Project.addProject', compact('project'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['project_name'] = $data['project_name'];
        $saveData['priority'] = $data['priority'];
        $saveData['project_url'] = $data['project_url'];
        $saveData['start_date'] = $data['start_date'];
        $saveData['end_date'] = $data['end_date'];
        $saveData['modules'] = $data['modules'];
        $saveData['type'] = $data['type'];
        $saveData['url'] = $data['url'];
        $saveData['git'] = $data['git'];
        $saveData['folder'] = $data['folder'];
        $saveData['about'] = $data['about'];
        $saveData['total_time'] = $data['total_time'];
        $saveData['total_amount'] = $data['total_amount'];
        $saveData['client_id'] = $data['client_id'];
        $saveData['language_id'] = $data['language_id'];
        $saveData['version'] = $data['version'];

        $row = Project::where('id', $id)->first();
        if ($row) {
            $Project = Project::where('id', $id)->update($saveData);
        }
        return view('admin.Project.listProject')->with('successMsg', 'Data has been updated.');
    }

    public function delete(Request $request)
    {
        $delete = Project::where('id', $request->id)->delete();
        return view('admin.Project.listProject');
    }

    public function createProjectJson(Request $request)
    {
    }
}
