<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Subject;
use App\Models\StudentClass;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
    public function index()
    {
        $data = Subject::paginate(20);
        return view('admin.Subject.listSubject', compact('data'));
    }

    public function add()
    {
        
$studentclasss = StudentClass::all();

        return view('admin.Subject.addSubject', compact('studentclasss'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['class_id'] = $data['class_id'];
$saveData['subject_name'] = $data['subject_name'];
$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];


        $Subject = Subject::create($saveData);

        // return response()->json(['success' => true, 'data' => $Subject], 200);
        return redirect('/admin/subject')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Subject::where('id', $id)->first();$studentclasss = StudentClass::all();
return view('admin.Subject.addSubject', compact('row', 'studentclasss'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('class_id')) {
               $path = request()->file('class_id')->store(
                   'file', 'public'
               );

               $data['class_id'] = \Storage::disk('public')->url($path);

            }$saveData['class_id'] = $data['class_id'];

 if (request()->hasFile('subject_name')) {
               $path = request()->file('subject_name')->store(
                   'file', 'public'
               );

               $data['subject_name'] = \Storage::disk('public')->url($path);

            }$saveData['subject_name'] = $data['subject_name'];

 if (request()->hasFile('slug')) {
               $path = request()->file('slug')->store(
                   'file', 'public'
               );

               $data['slug'] = \Storage::disk('public')->url($path);

            }$saveData['slug'] = $data['slug'];

 if (request()->hasFile('image')) {
               $path = request()->file('image')->store(
                   'file', 'public'
               );

               $data['image'] = \Storage::disk('public')->url($path);

            }$saveData['image'] = $data['image'];

        $row = Subject::where('id', $id)->first();
        if ($row){
            $Subject = Subject::where('id', $id)->update($saveData);
        }
        return redirect('/admin/subject')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Subject::where('id', $request->id)->delete();
        return redirect('/admin/subject');

    }


    public function getData(){
        $data = Subject::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
