<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectModuleUpload;
use App\Models\Project;
use App\Http\Controllers\Controller;
use App\Imports\ProjectModuleImport;
use Maatwebsite\Excel\Facades\Excel;

class ProjectModuleUploadController extends Controller
{
    public function index()
    {
        $data = ProjectModuleUpload::paginate(20);
        return view('admin.ProjectModuleUpload.listProjectModuleUpload', compact('data'));
    }

    public function add()
    {

        $projects = Project::all();

        return view('admin.ProjectModuleUpload.addProjectModuleUpload', compact('projects'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
        $saveData['project_id'] = $data['project_id'];

        if (request()->hasFile('filename')) {
            Excel::import(new ProjectModuleImport, request()->file('filename'));
            $path = request()->file('filename')->store(
                'file',
                'public'
            );

            $data['filename'] = \Storage::disk('public')->url($path);
        }
        $saveData['filename'] = $data['filename'];
        $saveData['uploaded_on'] = $data['uploaded_on'];


        $ProjectModuleUpload = ProjectModuleUpload::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectModuleUpload], 200);
        return redirect('/admin/project_module_upload')->with('successMsg', 'Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectModuleUpload::where('id', $id)->first();
        $projects = Project::all();
        return view('admin.ProjectModuleUpload.addProjectModuleUpload', compact('row', 'projects'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
        $saveData['project_id'] = $data['project_id'];
        $saveData['filename'] = $data['filename'];
        $saveData['uploaded_on'] = $data['uploaded_on'];

        $row = ProjectModuleUpload::where('id', $id)->first();
        if ($row) {
            $ProjectModuleUpload = ProjectModuleUpload::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectModuleUpload.listProjectModuleUpload')->with('successMsg', 'Data has been updated.');
    }

    public function delete($id, Request $request)
    {
        $delete = ProjectModuleUpload::where('id', $id)->delete();
        return redirect('/admin/project_module_upload');
    }
}
