<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Lead;
use App\Http\Controllers\Controller;

class LeadController extends Controller
{
    public function index()
    {
        $data = Lead::paginate(20);
        return view('admin.Lead.listLead', compact('data'));
    }

    public function add()
    {
        return view('admin.Lead.addLead');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['Lead_name'] = $data['Lead_name'];
$saveData['lead_by'] = $data['lead_by'];
$saveData['email'] = $data['email'];
$saveData['mobile'] = $data['mobile'];
$saveData['enquiry_for'] = $data['enquiry_for'];
$saveData['status'] = $data['status'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $Lead = Lead::create($saveData);

        // return response()->json(['success' => true, 'data' => $Lead], 200);
        return redirect('/admin/lead')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Lead::where('id', $id)->first();
        return view('admin.Lead.addLead', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['Lead_name'] = $data['Lead_name'];
$saveData['lead_by'] = $data['lead_by'];
$saveData['email'] = $data['email'];
$saveData['mobile'] = $data['mobile'];
$saveData['enquiry_for'] = $data['enquiry_for'];
$saveData['status'] = $data['status'];

        $row = Lead::where('id', $id)->first();
        if ($row){
            $Lead = Lead::where('id', $id)->update($saveData);
        }
        return view('admin.Lead.listLead')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Lead::where('id', $request->id)->delete();
        return view('admin.Lead.listLead');
    }
}
