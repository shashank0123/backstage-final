<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectProposal;
use App\Http\Controllers\Controller;

class ProjectProposalController extends Controller
{
    public function index()
    {
        $data = ProjectProposal::paginate(20);
        return view('admin.ProjectProposal.listProjectProposal', compact('data'));
    }

    public function add()
    {
        return view('admin.ProjectProposal.addProjectProposal');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['client_id'] = $data['client_id'];
$saveData['template_id'] = $data['template_id'];
$saveData['sent_on'] = $data['sent_on'];
$saveData['version'] = $data['version'];
$saveData['approval_status'] = $data['approval_status'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProjectProposal = ProjectProposal::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectProposal], 200);
        return redirect('/admin/project_proposal')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectProposal::where('id', $id)->first();
        return view('admin.ProjectProposal.addProjectProposal', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['client_id'] = $data['client_id'];
$saveData['template_id'] = $data['template_id'];
$saveData['sent_on'] = $data['sent_on'];
$saveData['version'] = $data['version'];
$saveData['approval_status'] = $data['approval_status'];

        $row = ProjectProposal::where('id', $id)->first();
        if ($row){
            $ProjectProposal = ProjectProposal::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectProposal.listProjectProposal')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectProposal::where('id', $request->id)->delete();
        return view('admin.ProjectProposal.listProjectProposal');
    }
}
