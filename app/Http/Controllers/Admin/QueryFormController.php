<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\QueryForm;
use App\Models\User;
use App\Models\StudentClass;
use App\Models\Subject;
use App\Models\Topic;
use App\Http\Controllers\Controller;

class QueryFormController extends Controller
{
    public function index()
    {
        $data = QueryForm::paginate(20);
        return view('admin.QueryForm.listQueryForm', compact('data'));
    }

    public function add()
    {
        
$users = User::all();
$studentclasss = StudentClass::all();
$subjects = Subject::all();
$topics = Topic::all();

        return view('admin.QueryForm.addQueryForm', compact('users','studentclasss','subjects','topics'));
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['class_id'] = $data['class_id'];
$saveData['subject_id'] = $data['subject_id'];
$saveData['topic_id'] = $data['topic_id'];
$saveData['time'] = $data['time'];
$saveData['address'] = $data['address'];
$saveData['mobile'] = $data['mobile'];
$saveData['email'] = $data['email'];
$saveData['school'] = $data['school'];


        $QueryForm = QueryForm::create($saveData);

        // return response()->json(['success' => true, 'data' => $QueryForm], 200);
        return redirect('/admin/queryform')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = QueryForm::where('id', $id)->first();$users = User::all();
$studentclasss = StudentClass::all();
$subjects = Subject::all();
$topics = Topic::all();
return view('admin.QueryForm.addQueryForm', compact('row', 'users','studentclasss','subjects','topics'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['user_id'] = $data['user_id'];
$saveData['class_id'] = $data['class_id'];
$saveData['subject_id'] = $data['subject_id'];
$saveData['topic_id'] = $data['topic_id'];
$saveData['time'] = $data['time'];
$saveData['address'] = $data['address'];
$saveData['mobile'] = $data['mobile'];
$saveData['email'] = $data['email'];
$saveData['school'] = $data['school'];

        $row = QueryForm::where('id', $id)->first();
        if ($row){
            $QueryForm = QueryForm::where('id', $id)->update($saveData);
        }
        return redirect('/admin/queryform')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = QueryForm::where('id', $request->id)->delete();
        return redirect('/admin/queryform');

    }


    public function getData(){
        $data = QueryForm::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
