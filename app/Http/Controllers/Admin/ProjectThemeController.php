<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ProjectTheme;
use App\Http\Controllers\Controller;

class ProjectThemeController extends Controller
{
    public function index()
    {
        $data = ProjectTheme::paginate(20);
        return view('admin.ProjectTheme.listProjectTheme', compact('data'));
    }

    public function add()
    {
        return view('admin.ProjectTheme.addProjectTheme');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['theme_id'] = $data['theme_id'];
$saveData['installed_on'] = $data['installed_on'];
$saveData['subscription'] = $data['subscription'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ProjectTheme = ProjectTheme::create($saveData);

        // return response()->json(['success' => true, 'data' => $ProjectTheme], 200);
        return redirect('/admin/projecttheme')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ProjectTheme::where('id', $id)->first();
        return view('admin.ProjectTheme.addProjectTheme', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['project_id'] = $data['project_id'];
$saveData['theme_id'] = $data['theme_id'];
$saveData['installed_on'] = $data['installed_on'];
$saveData['subscription'] = $data['subscription'];

        $row = ProjectTheme::where('id', $id)->first();
        if ($row){
            $ProjectTheme = ProjectTheme::where('id', $id)->update($saveData);
        }
        return view('admin.ProjectTheme.listProjectTheme')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ProjectTheme::where('id', $request->id)->delete();
        return view('admin.ProjectTheme.listProjectTheme');
    }
}
