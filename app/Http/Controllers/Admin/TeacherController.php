<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Teacher;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    public function index()
    {
        $data = Teacher::paginate(20);
        return view('admin.Teacher.listTeacher', compact('data'));
    }

    public function add()
    {
        

        return view('admin.Teacher.addTeacher', compact());
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['career_start'] = $data['career_start'];
$saveData['title'] = $data['title'];
$saveData['bio'] = $data['bio'];

 if (request()->hasFile('profile_pic')) {
               $path = request()->file('profile_pic')->store(
                   'file', 'public'
               );

               $data['profile_pic'] = \Storage::disk('public')->url($path);

            }$saveData['profile_pic'] = $data['profile_pic'];

 if (request()->hasFile('resume')) {
               $path = request()->file('resume')->store(
                   'file', 'public'
               );

               $data['resume'] = \Storage::disk('public')->url($path);

            }$saveData['resume'] = $data['resume'];


        $Teacher = Teacher::create($saveData);

        // return response()->json(['success' => true, 'data' => $Teacher], 200);
        return redirect('/admin/teacher')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = Teacher::where('id', $id)->first();return view('admin.Teacher.addTeacher', compact('row', ));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];

 if (request()->hasFile('career_start')) {
               $path = request()->file('career_start')->store(
                   'file', 'public'
               );

               $data['career_start'] = \Storage::disk('public')->url($path);

            }$saveData['career_start'] = $data['career_start'];

 if (request()->hasFile('title')) {
               $path = request()->file('title')->store(
                   'file', 'public'
               );

               $data['title'] = \Storage::disk('public')->url($path);

            }$saveData['title'] = $data['title'];

 if (request()->hasFile('bio')) {
               $path = request()->file('bio')->store(
                   'file', 'public'
               );

               $data['bio'] = \Storage::disk('public')->url($path);

            }$saveData['bio'] = $data['bio'];

 if (request()->hasFile('profile_pic')) {
               $path = request()->file('profile_pic')->store(
                   'file', 'public'
               );

               $data['profile_pic'] = \Storage::disk('public')->url($path);

            }$saveData['profile_pic'] = $data['profile_pic'];

 if (request()->hasFile('resume')) {
               $path = request()->file('resume')->store(
                   'file', 'public'
               );

               $data['resume'] = \Storage::disk('public')->url($path);

            }$saveData['resume'] = $data['resume'];

        $row = Teacher::where('id', $id)->first();
        if ($row){
            $Teacher = Teacher::where('id', $id)->update($saveData);
        }
        return redirect('/admin/teacher')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = Teacher::where('id', $request->id)->delete();
        return redirect('/admin/teacher');

    }


    public function getData(){
        $data = Teacher::all();
        return response()->json(['data' => $data, 'success' => true, 'message' => 'data retrieved']);
    }
}
