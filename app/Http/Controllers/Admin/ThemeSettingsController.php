<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\ThemeSettings;
use App\Http\Controllers\Controller;

class ThemeSettingsController extends Controller
{
    public function index()
    {
        $data = ThemeSettings::paginate(20);
        return view('admin.ThemeSettings.listThemeSettings', compact('data'));
    }

    public function add()
    {
        return view('admin.ThemeSettings.addThemeSettings');
    }

    public function store(Request $request)
    {
        //$validator = Validator::make(request()->all(),
        //            [
        //                'category_name' => 'required',
        //            ]);

        //if ($validator->fails()) {
        //    return response()->json(['error' => $validator->messages()->first()], 500);
        //}

        $data = request()->all();
        $saveData = [];
$saveData['theme_id'] = $data['theme_id'];
$saveData['file_locations'] = $data['file_locations'];
$saveData['layout'] = $data['layout'];
$saveData['side_bar'] = $data['side_bar'];
$saveData['headers'] = $data['headers'];
//if (request()->hasFile('category_image')) {
        //    $path = request()->file('category_image')->store(
        //        'photos', 'public'
        //    );

        //    $data['category_image'] = \Storage::disk('public')->url($path);

        //}

        $ThemeSettings = ThemeSettings::create($saveData);

        // return response()->json(['success' => true, 'data' => $ThemeSettings], 200);
        return redirect('/admin/themesettings')->with('successMsg','Data has been saved.');
    }

    public function edit($id)
    {
        $row = ThemeSettings::where('id', $id)->first();
        return view('admin.ThemeSettings.addThemeSettings', compact('row'));
    }

    public function update($id, Request $request)
    {
        $data = request()->all();
        $saveData = [];
$saveData['theme_id'] = $data['theme_id'];
$saveData['file_locations'] = $data['file_locations'];
$saveData['layout'] = $data['layout'];
$saveData['side_bar'] = $data['side_bar'];
$saveData['headers'] = $data['headers'];

        $row = ThemeSettings::where('id', $id)->first();
        if ($row){
            $ThemeSettings = ThemeSettings::where('id', $id)->update($saveData);
        }
        return view('admin.ThemeSettings.listThemeSettings')->with('successMsg','Data has been updated.');

    }

    public function delete(Request $request)
    {
        $delete = ThemeSettings::where('id', $request->id)->delete();
        return view('admin.ThemeSettings.listThemeSettings');
    }
}
