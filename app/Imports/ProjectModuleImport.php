<?php

namespace App\Imports;

use App\Models\ProjectModule;
use Maatwebsite\Excel\Concerns\ToModel;

class ProjectModuleImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new ProjectModule([
            'project_id'     => $row[0],
            'module_name'     => $row[1],
            'model_name'     => $row[2],
            'db_fields'     => $row[3],
            'db_field_type'     => $row[4],
            'theme_id'     => $row[5],
            'feature_for'     => $row[6],
            'controller_name'     => $row[7],
            'view_folder_name'     => $row[8],
            'options_to_add'     => $row[9],
            'status'     => $row[10],
            'is_template'     => $row[11],
            'is_processed'     => $row[12]

        ]);
    }
}
