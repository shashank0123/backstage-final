<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
protected $fillable = ["career_start","title","bio","profile_pic","resume"];
}