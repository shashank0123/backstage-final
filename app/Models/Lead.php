<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
protected $fillable = ["Lead_name","lead_by","email","mobile","enquiry_for","status"];
}