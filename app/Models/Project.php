<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ["project_name", "priority", "project_url", "start_date", "end_date", "modules", "type", "url", "git", "folder", "about", "total_time", "total_amount", "client_id", "language_id", "version"];
}
