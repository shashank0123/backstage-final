<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProposalTemplate extends Model
{
protected $fillable = ["template_name","template_for","template_tag","template_file","created_by","status","company_id"];
}