<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ThemeElement extends Model
{
protected $fillable = ["theme_id","element_name","code","variables"];
}