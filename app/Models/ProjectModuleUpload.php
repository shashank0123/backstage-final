<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectModuleUpload extends Model
{
protected $fillable = ["project_id","filename","uploaded_on"];
}