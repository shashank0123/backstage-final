<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectProposal extends Model
{
protected $fillable = ["project_id","client_id","template_id","sent_on","version","approval_status"];
}