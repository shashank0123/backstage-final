<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectTheme extends Model
{
protected $fillable = ["project_id","theme_id","installed_on","subscription"];
}