<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class SmsNotification extends Model
{
protected $fillable = ["notification_type","title","message","status","exec_status"];
}