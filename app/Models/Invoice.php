<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
protected $fillable = ["invoice_no","company_id","invoice_template_id","invoice_for","address","amount","due_date","extra_details"];
}