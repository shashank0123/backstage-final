<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ThemeSettings extends Model
{
protected $fillable = ["theme_id","file_locations","layout","side_bar","headers"];
}