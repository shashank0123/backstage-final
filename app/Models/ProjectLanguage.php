<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProjectLanguage extends Model
{
protected $fillable = ["project_id","language_id","version"];
}