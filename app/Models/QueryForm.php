<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class QueryForm extends Model
{
protected $fillable = ["user_id","class_id","subject_id","topic_id","time","address","mobile","email","school"];
}