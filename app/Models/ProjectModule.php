<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectModule extends Model
{
    protected $fillable = ["project_id", "module_name", "model_name", "db_fields", "db_field_type", "theme_id", "feature_for", "controller_name", "view_folder_name", "options_to_add", "status", "is_template", "is_processed"];
}
