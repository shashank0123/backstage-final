<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeSettingsTable extends Migration

{
    public function up()
    {
        Schema::create('theme_settings', function (Blueprint $table) {
            $table->id();
            $table->string('theme_id')->default('0');
            $table->string('file_locations')->default('0');
            $table->string('layout')->default('0');
            $table->string('side_bar')->default('0');
            $table->string('headers')->default('0');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('theme_settings ');
    }
}
