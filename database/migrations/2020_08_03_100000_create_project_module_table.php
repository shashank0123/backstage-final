<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectModuleTable extends Migration

{
    public function up()
    {
        Schema::create('project_modules', function (Blueprint $table) {
            $table->id();
            $table->string('project_id')->nullable();
            $table->string('module_name')->nullable();
            $table->string('model_name')->nullable();
            $table->string('db_fields')->nullable();
            $table->string('db_fields_type')->nullable();
            $table->string('theme_id')->nullable();
            $table->string('feature_for')->nullable();
            $table->string('controller_name')->nullable();
            $table->string('view_folder_name')->nullable();
            $table->string('options_to_add')->nullable();
            $table->string('status')->nullable();
            $table->string('is_template')->nullable();
            $table->string('is_processed')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_modules ');
    }
}
