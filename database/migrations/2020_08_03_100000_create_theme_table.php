<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeTable extends Migration

{
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('type')->nullable();
            $table->string('deployment_date')->nullable();
            $table->string('version')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('themes ');
    }
}
