<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectLanguageTable extends Migration

{
    public function up()
    {
        Schema::create('project_languages', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->default('0');
$table->string('language_id')->default('0');
$table->string('version')->default('0');
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_languages ');
    }
}
