<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectThemeTable extends Migration

{
    public function up()
    {
        Schema::create('projectthemes', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->default('0');
$table->string('theme_id')->default('0');
$table->string('installed_on')->default('0');
$table->string('subscription')->default('0');
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('projectthemes ');
    }
}
