<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectProposalTable extends Migration

{
    public function up()
    {
        Schema::create('project_proposals', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('client_id')->nullable();
$table->string('template_id')->nullable();
$table->string('sent_on')->nullable();
$table->string('version')->nullable();
$table->string('approval_status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_proposals');
    }
}
