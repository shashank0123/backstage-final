<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProposalTemplateTable extends Migration

{
    public function up()
    {
        Schema::create('proposal_templates', function (Blueprint $table) {
            $table->id();
$table->string('template_name')->nullable();
$table->string('template_for')->nullable();
$table->string('template_tag')->nullable();
$table->string('template_file')->nullable();
$table->string('created_by')->nullable();
$table->string('status')->nullable();
$table->string('company_id')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('proposal_templates');
    }
}
