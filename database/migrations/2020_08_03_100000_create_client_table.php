<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientTable extends Migration

{
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
$table->string('name')->default('0');
$table->string('email')->default('0');
$table->string('phone')->default('0');
$table->string('address')->default('0');
$table->string('company')->default('0');
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('clients ');
    }
}
