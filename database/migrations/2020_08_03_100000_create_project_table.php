<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration

{
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->id();
            $table->string('project_name')->default('0');
            $table->string('priority')->default('0');
            $table->string('project_url')->default('0');
            $table->string('start_date')->default('0');
            $table->string('end_date')->default('0');
            $table->string('modules')->default('0');
            $table->string('type')->default('0');
            $table->string('url')->default('0');
            $table->string('git')->default('0');
            $table->string('folder')->default('0');
            $table->string('about')->default('0');
            $table->string('total_time')->default('0');
            $table->string('total_amount')->default('0');
            $table->string('client_id')->default('0');
            $table->string('language_id')->default('0');
            $table->string('version')->default('0');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('projects ');
    }
}
