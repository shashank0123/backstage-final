<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadTable extends Migration

{
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->id();
$table->string('Lead_name')->nullable();
$table->string('lead_by')->nullable();
$table->string('email')->nullable();
$table->string('mobile')->nullable();
$table->string('enquiry_for')->nullable();
$table->string('status')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
