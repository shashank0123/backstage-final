<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThemeElementsTable extends Migration

{
    public function up()
    {
        Schema::create('theme_elements', function (Blueprint $table) {
            $table->id();
            $table->string('theme_id')->nullable();
            $table->string('element_name')->nullable();
            $table->text('code')->nullable();
            $table->string('variables')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('theme_elements ');
    }
}
