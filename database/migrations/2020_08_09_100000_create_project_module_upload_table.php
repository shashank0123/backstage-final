<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectModuleUploadTable extends Migration

{
    public function up()
    {
        Schema::create('project_module_uploads', function (Blueprint $table) {
            $table->id();
            $table->string('project_id')->nullable();
            $table->string('filename')->nullable();
            $table->string('uploaded_on')->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_module_uploads');
    }
}
