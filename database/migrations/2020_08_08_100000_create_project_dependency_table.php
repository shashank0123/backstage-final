<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectDependencyTable extends Migration

{
    public function up()
    {
        Schema::create('project_dependencys', function (Blueprint $table) {
            $table->id();
$table->string('project_id')->nullable();
$table->string('dependency_id')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('project_dependencys');
    }
}
