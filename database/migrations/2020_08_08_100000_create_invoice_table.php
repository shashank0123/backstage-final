<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceTable extends Migration

{
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
$table->string('invoice_no')->nullable();
$table->string('company_id')->nullable();
$table->string('invoice_template_id')->nullable();
$table->string('invoice_for')->nullable();
$table->string('address')->nullable();
$table->string('amount')->nullable();
$table->string('due_date')->nullable();
$table->string('extra_details')->nullable();
$table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
