@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/project_language/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Id</label>
                <input class='form-control btn-square' type='text' name='project_id' placeholder='Enter Project Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Language Id</label>
                <input class='form-control btn-square' type='text' name='language_id' placeholder='Enter Language Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Version</label>
                <input class='form-control btn-square' type='text' name='version' placeholder='Enter Version'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project_language' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection