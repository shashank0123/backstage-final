@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/themesettings/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>ThemeSettings </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='display' id='advance-1'>
                        <thead>
                          <tr><th>Theme Id</th><th>File Locations</th><th>Layout</th><th>Side Bar</th><th>Headers</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->theme_id}}</td><td>{{$value->file_locations}}</td><td>{{$value->layout}}</td><td>{{$value->side_bar}}</td><td>{{$value->headers}}</td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection