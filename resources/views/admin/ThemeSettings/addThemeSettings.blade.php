@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/themesettings/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Theme Id</label>
                <input class='form-control btn-square' type='text' name='theme_id' placeholder='Enter Theme Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>File Locations</label>
                <input class='form-control btn-square' type='text' name='file_locations' placeholder='Enter File Locations'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Layout</label>
                <input class='form-control btn-square' type='text' name='layout' placeholder='Enter Layout'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Side Bar</label>
                <input class='form-control btn-square' type='text' name='side_bar' placeholder='Enter Side Bar'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Headers</label>
                <input class='form-control btn-square' type='text' name='headers' placeholder='Enter Headers'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/themesettings' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection