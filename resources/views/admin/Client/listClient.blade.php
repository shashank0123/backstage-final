@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/client/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Client </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='display' id='advance-1'>
                        <thead>
                          <tr><th>Name</th><th>Email</th><th>Phone</th><th>Address</th><th>Company</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->name}}</td><td>{{$value->email}}</td><td>{{$value->phone}}</td><td>{{$value->address}}</td><td>{{$value->company}}</td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection