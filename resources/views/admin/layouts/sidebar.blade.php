<nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="/admin/home"><i data-feather="home"></i><span>Dashboard</span></a></li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Project</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/project/add">Create Project</a></li>
                      <li><a href="/admin/project">Projects</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Project Module</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/project_module/add">Create Project Module</a></li>
                      <li><a href="/admin/project_module">Projects Module</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Project Dependency</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/project_dependency/add">Create Project Dependency</a></li>
                      <li><a href="/admin/project_dependency">Project Dependency</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Project Language</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/project_language/add">Create Language</a></li>
                      <li><a href="/admin/project_language">Languages</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Theme</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/theme/add">Create Theme</a></li>
                      <li><a href="/admin/theme">Themes</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Theme Element</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/themeelement/add">Create Theme Element</a></li>
                      <li><a href="/admin/themeelement">Themes Element</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Theme Settings</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/themesettings/add">Create Theme Setting</a></li>
                      <li><a href="/admin/themesettings">Themes Setting</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Client</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/client/add">Create Client</a></li>
                      <li><a href="/admin/client">Client</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Slider</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/slider/add">Create Slider</a></li>
                      <li><a href="/admin/slider">Slider</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Project Proposal</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/project_proposal/add">Create Project Proposal</a></li>
                      <li><a href="/admin/project_proposal">Project Proposal</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Proposal Template</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/proposal_template/add">Create Proposal Template</a></li>
                      <li><a href="/admin/proposal_template">Proposal Template</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="folder-plus"></i><span>Leads</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/lead/add">Create Lead</a></li>
                      <li><a href="/admin/lead">Leads</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
