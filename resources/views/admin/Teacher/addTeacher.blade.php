@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/teacher/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Career Start</label>
                <input class='form-control btn-square' type='text' name='career_start' placeholder='Enter Career Start'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Title</label>
                <input class='form-control btn-square' type='text' name='title' placeholder='Enter Title'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Bio</label>
                <input class='form-control btn-square' type='text' name='bio' placeholder='Enter Bio'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='profile_pic'>File</label>
                    <div class='col-lg-12'>
                        <input id='profile_pic' name='profile_pic' class='input-file' type='file'>
                    </div>
                    </div><div class='form-group row'>
                    <label class='col-lg-12 control-label text-lg-left' for='resume'>File</label>
                    <div class='col-lg-12'>
                        <input id='resume' name='resume' class='input-file' type='file'>
                    </div>
                    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/teacher' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection