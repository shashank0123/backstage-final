@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/project_module/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Project Module </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Project Id</th><th>Module Name</th><th>Model Name</th><th>Db Fields</th><th>Theme Id</th><th>Feature For</th><th>Controller Name</th><th>View Folder Name</th><th>Options To Add</th><th>Status</th><th>Is Template</th><th>Is Processed</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value)
                        <tr>
                            <td>{{$value->project_id}}</td>
                            <td>{{$value->module_name}}</td>
                            <td>{{$value->model_name}}</td>
                            <td>{{$value->db_fields}}</td>
                            <td>{{$value->theme_id}}</td>
                            <td>{{$value->feature_for}}</td>
                            <td>{{$value->controller_name}}</td>
                            <td>{{$value->view_folder_name}}</td>
                            <td>{{$value->options_to_add}}</td>
                            <td>{{$value->status}}</td>
                            <td>{{$value->is_template}}</td>
                            <td>{{$value->is_processed}}</td>
                        </tr>
                        @endforeach
                    </tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection
