@extends('admin.layouts.admin')
@section('content')
<Form action='/admin/project_module/add' method='post'>
    @csrf
    <div class="col-md-12">
        <div class="form-group ui-draggable-handle" style="position: static;">
            <label for="formcontrol-select1">Project</label>
            <select class="form-control btn-square" name="project_id" id="project_id">
                <option value="0">Backstage</option>
                @foreach ($projects as $item)
                    <option value="{{$item->id}}">{{$item->project_name}}</option>
                @endforeach

            </select>
            <p style="display: none" class="help-block">Error Message</p>
        </div>
    </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Module Name</label>
                <input class='form-control btn-square' type='text' name='module_name' placeholder='Enter Module Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Db Fields</label>
                <input class='form-control btn-square' type='text' name='db_fields' placeholder='Enter Db Fields'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Db Field Types</label>
                <input class='form-control btn-square' type='text' name='db_field_type' placeholder='Enter Db Fields'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class="col-md-12">
        <div class="form-group ui-draggable-handle" style="position: static;">
            <label for="formcontrol-select1">Theme</label>
            <select class="form-control btn-square" name="theme_id" id="theme_id">
                <option value="0">Default</option>
                @foreach ($themes as $item)
                    <option value="{{$item->id}}">{{$item->name}}</option>
                @endforeach

            </select>
            <p style="display: none" class="help-block">Error Message</p>
        </div>
    </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Feature For</label>
                <input class='form-control btn-square' type='text' name='feature_for' placeholder='Enter Feature For'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Controller Name</label>
                <input class='form-control btn-square' type='text' name='controller_name' placeholder='Enter Controller Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>View Folder Name</label>
                <input class='form-control btn-square' type='text' name='view_folder_name' placeholder='Enter View Folder Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Options To Add</label>
                <input class='form-control btn-square' type='text' name='options_to_add' placeholder='Enter Options To Add'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Is Template</label>
                <input class='form-control btn-square' type='text' name='is_template' placeholder='Enter Is Template'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Is Processed</label>
                <input class='form-control btn-square' type='text' name='is_processed' placeholder='Enter Is Processed'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project_module' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection
