@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/themeelement/add' method='post'>@csrf
    <div class="col-md-12">
        <div class="form-group ui-draggable-handle" style="position: static;">
            <label for="formcontrol-select1">Theme</label>
            <select class="form-control btn-square" name="theme_id" id="theme_id">
                @foreach ($theme as $item)
                    <option value="{{$item->id}}">{{$item->connector_name}}</option>
                @endforeach
            </select>
            <p style="display: none" class="help-block">Error Message</p>
        </div>
    </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Element Name</label>
                <input class='form-control btn-square' type='text' name='element_name' placeholder='Enter Element Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Code</label>
                <input class='form-control btn-square' type='text' name='code' placeholder='Enter Code'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Variables</label>
                <input class='form-control btn-square' type='text' name='variables' placeholder='Enter Variables'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/themeelement' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection
