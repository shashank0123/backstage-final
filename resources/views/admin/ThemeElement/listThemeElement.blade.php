@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/themeelement/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>ThemeElement </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='display' id='advance-1'>
                        <thead>
                          <tr><th>Theme Id</th><th>Element Name</th><th>Code</th><th>Variables</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->theme_id}}</td><td>{{$value->element_name}}</td><td>{{$value->code}}</td><td>{{$value->variables}}</td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection