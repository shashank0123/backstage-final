@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/teachersubject/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Teacher</label>
            <select class='form-control btn-square' name='teacher_id' id='teacher_id'>
                @foreach ($teachers as $item)
                    <option value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Subject</label>
            <select class='form-control btn-square' name='subject_id' id='subject_id'>
                @foreach ($subjects as $item)
                    <option value='{{$item->id}}'>{{$item->subject_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/teachersubject' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection