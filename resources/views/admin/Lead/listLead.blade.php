@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/lead/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Lead </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Lead Name</th><th>Lead By</th><th>Email</th><th>Mobile</th><th>Enquiry For</th><th>Status</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->Lead_name}}</td><td>{{$value->lead_by}}</td><td>{{$value->email}}</td><td>{{$value->mobile}}</td><td>{{$value->enquiry_for}}</td><td>{{$value->status}}</td><td><a href='/admin/lead/edit/$value->id' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/lead/delete/$value->id' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection