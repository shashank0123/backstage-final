@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/lead/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Lead Name</label>
                <input class='form-control btn-square' type='text' name='Lead_name' placeholder='Enter Lead Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Lead By</label>
                <input class='form-control btn-square' type='text' name='lead_by' placeholder='Enter Lead By'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Email</label>
                <input class='form-control btn-square' type='text' name='email' placeholder='Enter Email'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Mobile</label>
                <input class='form-control btn-square' type='text' name='mobile' placeholder='Enter Mobile'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Enquiry For</label>
                <input class='form-control btn-square' type='text' name='enquiry_for' placeholder='Enter Enquiry For'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/lead' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection