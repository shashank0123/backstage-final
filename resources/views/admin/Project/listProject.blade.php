@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/project/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Project </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr>
                              <th>Project Name</th>
                              <th>Priority</th>
                              <th>Project Url</th>
                              <th>Start Date</th>
                              <th>End Date</th>
                              <th>Modules</th>
                              <th>Type</th>
                              <th>Url</th>
                              <th>Git</th>
                              <th>Folder</th>
                              <th>About</th>
                              <th>Total Time</th>
                              <th>Total Amount</th>
                              <th>Client Id</th>
                              <th>Language Id</th>
                              <th>Version</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value)
                        <tr>
                            <td>{{$value->project_name}}</td>
                            <td>{{$value->priority}}</td>
                            <td>{{$value->project_url}}</td>
                            <td>{{$value->start_date}}</td>
                            <td>{{$value->end_date}}</td>
                            <td>{{$value->modules}}</td>
                            <td>{{$value->type}}</td>
                            <td>{{$value->url}}</td>
                            <td>{{$value->git}}</td>
                            <td>{{$value->folder}}</td>
                            <td>{{$value->about}}</td>
                            <td>{{$value->total_time}}</td>
                            <td>{{$value->total_amount}}</td>
                            <td>{{$value->client_id}}</td>
                            <td>{{$value->language_id}}</td>
                            <td>{{$value->version}}</td>
                            <td><a href='/admin/project/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/project/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td>
                        </tr>
                        @endforeach
                    </tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection
