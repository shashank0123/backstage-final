@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/project/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Name</label>
<input class='form-control btn-square' type='text' value="@if (isset($project->project_name)) {{$project->project_name}} @endif" name='project_name' placeholder='Enter Project Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Priority</label>
                <input class='form-control btn-square' type='text' name='priority' placeholder='Enter Priority'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Url</label>
                <input class='form-control btn-square' type='text' name='project_url' placeholder='Enter Project Url'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
            <div class='form-group input-group'>
                            <label class='col-lg-12 control-label text-lg-left' for='$value'>$value</label>
                              <input class='datepicker-here form-control digits' name='$value' type='text' data-language='en' data-original-title=' title='>
                            </div>
            <div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>End Date</label>
                <input class='form-control btn-square' type='text' name='end_date' placeholder='Enter End Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Modules</label>
                <input class='form-control btn-square' type='text' name='modules' placeholder='Enter Modules'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Type</label>
                <input class='form-control btn-squa
                re' type='text' name='type' placeholder='Enter Type'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Url</label>
                <input class='form-control btn-square' type='text' name='url' placeholder='Enter Url'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Git</label>
                <input class='form-control btn-square' type='text' name='git' placeholder='Enter Git'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Folder</label>
                <input class='form-control btn-square' type='text' name='folder' placeholder='Enter Folder'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>About</label>
                <input class='form-control btn-square' type='text' name='about' placeholder='Enter About'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Total Time</label>
                <input class='form-control btn-square' type='text' name='total_time' placeholder='Enter Total Time'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Total Amount</label>
                <input class='form-control btn-square' type='text' name='total_amount' placeholder='Enter Total Amount'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Client Id</label>
                <input class='form-control btn-square' type='text' name='client_id' placeholder='Enter Client Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Language Id</label>
                <input class='form-control btn-square' type='text' name='language_id' placeholder='Enter Language Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Version</label>
                <input class='form-control btn-square' type='text' name='version' placeholder='Enter Version'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection
