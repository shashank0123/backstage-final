@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/queryform/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>QueryForm </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>User Id</th><th>Class Id</th><th>Subject Id</th><th>Topic Id</th><th>Time</th><th>Address</th><th>Mobile</th><th>Email</th><th>School</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->user_id}}</td><td>{{$value->class_id}}</td><td>{{$value->subject_id}}</td><td>{{$value->topic_id}}</td><td>{{$value->time}}</td><td>{{$value->address}}</td><td>{{$value->mobile}}</td><td>{{$value->email}}</td><td>{{$value->school}}</td><td><a href='/admin/queryform/edit/{{$value->id}}' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/queryform/delete/{{$value->id}}' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection