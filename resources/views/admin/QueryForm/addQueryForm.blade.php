@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''>
<Form action='/admin/queryform/add' method='post' enctype='multipart/form-data'>
@csrf
<div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>User</label>
            <select class='form-control btn-square' name='user_id' id='user_id'>
                @foreach ($users as $item)
                    <option value='{{$item->id}}'>{{$item->name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>StudentClass</label>
            <select class='form-control btn-square' name='class_id' id='class_id'>
                @foreach ($studentclasss as $item)
                    <option value='{{$item->id}}'>{{$item->class_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Subject</label>
            <select class='form-control btn-square' name='subject_id' id='subject_id'>
                @foreach ($subjects as $item)
                    <option value='{{$item->id}}'>{{$item->subject_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='col-md-12'>
        <div class='form-group ui-draggable-handle' style='position: static;'>
            <label for='formcontrol-select1'>Topic</label>
            <select class='form-control btn-square' name='topic_id' id='topic_id'>
                @foreach ($topics as $item)
                    <option value='{{$item->id}}'>{{$item->topic_name}}</option>
                @endforeach

            </select>
            <p style='display: none' class='help-block'>Error Message</p>
        </div>
    </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Time</label>
                <input class='form-control btn-square' type='text' name='time' placeholder='Enter Time'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address</label>
                <input class='form-control btn-square' type='text' name='address' placeholder='Enter Address'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Mobile</label>
                <input class='form-control btn-square' type='text' name='mobile' placeholder='Enter Mobile'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Email</label>
                <input class='form-control btn-square' type='text' name='email' placeholder='Enter Email'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>School</label>
                <input class='form-control btn-square' type='text' name='school' placeholder='Enter School'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div>
</div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/queryform' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection