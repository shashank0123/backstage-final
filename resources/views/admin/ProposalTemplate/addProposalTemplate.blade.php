@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/proposal_template/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Template Name</label>
                <input class='form-control btn-square' type='text' name='template_name' placeholder='Enter Template Name'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Template For</label>
                <input class='form-control btn-square' type='text' name='template_for' placeholder='Enter Template For'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Template Tag</label>
                <input class='form-control btn-square' type='text' name='template_tag' placeholder='Enter Template Tag'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Template File</label>
                <input class='form-control btn-square' type='text' name='template_file' placeholder='Enter Template File'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Created By</label>
                <input class='form-control btn-square' type='text' name='created_by' placeholder='Enter Created By'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Status</label>
                <input class='form-control btn-square' type='text' name='status' placeholder='Enter Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Company Id</label>
                <input class='form-control btn-square' type='text' name='company_id' placeholder='Enter Company Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/proposal_template' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection