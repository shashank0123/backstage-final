@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/proposal_template/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Proposal Template </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Template Name</th><th>Template For</th><th>Template Tag</th><th>Template File</th><th>Created By</th><th>Status</th><th>Company Id</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->template_name}}</td><td>{{$value->template_for}}</td><td>{{$value->template_tag}}</td><td>{{$value->template_file}}</td><td>{{$value->created_by}}</td><td>{{$value->status}}</td><td>{{$value->company_id}}</td><td><a href='/admin/proposal_template/edit/$value->id' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/proposal_template/delete/$value->id' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection