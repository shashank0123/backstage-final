@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/invoice/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Invoice No</label>
                <input class='form-control btn-square' type='text' name='invoice_no' placeholder='Enter Invoice No'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Company Id</label>
                <input class='form-control btn-square' type='text' name='company_id' placeholder='Enter Company Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Invoice Template Id</label>
                <input class='form-control btn-square' type='text' name='invoice_template_id' placeholder='Enter Invoice Template Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Invoice For</label>
                <input class='form-control btn-square' type='text' name='invoice_for' placeholder='Enter Invoice For'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Address</label>
                <input class='form-control btn-square' type='text' name='address' placeholder='Enter Address'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Amount</label>
                <input class='form-control btn-square' type='text' name='amount' placeholder='Enter Amount'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Due Date</label>
                <input class='form-control btn-square' type='text' name='due_date' placeholder='Enter Due Date'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Extra Details</label>
                <input class='form-control btn-square' type='text' name='extra_details' placeholder='Enter Extra Details'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/invoice' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection