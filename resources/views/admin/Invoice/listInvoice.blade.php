@extends('admin.layouts.admin')
@section('content')
<div class='col-sm-12'>
                <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <a href='/admin/invoice/add' class='btn btn-secondary' data-original-title='Add New' title=''>Add New</a>
            <p class='help-block'>Press Submit to save</p>
        </div>
    </div>
                <div class='card'>
                  <div class='card-header'>
                    <h5>Invoice </h5>

                  </div>
                  <div class='card-body'>
                    <div class='table-responsive'>
                      <table class='table table-styling' id='advance-1'>
                        <thead>
                          <tr><th>Invoice No</th><th>Company Id</th><th>Invoice Template Id</th><th>Invoice For</th><th>Address</th><th>Amount</th><th>Due Date</th><th>Extra Details</th><th>Action</th></tr>
                        </thead>
                        <tbody>
                        @foreach ($data as  $value) {
                        <tr><td>{{$value->invoice_no}}</td><td>{{$value->company_id}}</td><td>{{$value->invoice_template_id}}</td><td>{{$value->invoice_for}}</td><td>{{$value->address}}</td><td>{{$value->amount}}</td><td>{{$value->due_date}}</td><td>{{$value->extra_details}}</td><td><a href='/admin/invoice/edit/$value->id' class='btn btn-primary' data-original-title='' title=''>Edit</a><a href='/admin/invoice/delete/$value->id' class='btn btn-danger' data-original-title='' title=''>delete</a></td></tr>@endforeach</tbody>


                      </table>
                    </div>
                  </div>
                </div>
              </div>
@endsection