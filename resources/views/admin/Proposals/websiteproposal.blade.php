<p><strong>Introduction</strong></p>
<p><strong>&nbsp;</strong></p>
<p>Thank you for your interest in partnering with Backstage Supporters Pvt. Ltd. for your project. With well over 100,000 firms offering IT services, we Know how challenging it can be to find the right agency for your web Development needs.</p>
<p>At Backstage Supporters Pvt. Ltd. we hold one goal above all others - 100% client satisfaction. Our in-house team of android developers, graphic Designers, UI designer, content writers and developers uphold the highest Standards for project planning and execution, and we're dedicated to building the perfect product for your company on-time and on-budget.</p>
<p>We've built products for several brands around the world with great success, and are quite excited to get to work on yours.&nbsp;</p>
<p>In this proposal, you'll find what we feel is the optimal solution for your product needs, along with the associated delivery timeline, costs, and project terms. Once you've reviewed this proposal thoroughly, simply sign it at the bottom to indicate your approval.</p>
<p>&nbsp;</p>
<p>Thanks again for the opportunity to earn your business.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Vibrant Concepts </strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Timespan</strong> : 2 Month&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
<p><strong>Start date</strong> :&nbsp; 10th of August, 2020(Tentative)&nbsp;</p>
<p><strong>Deliverables :</strong></p>
<ul>
<li>Admin Panel</li>
<li>Website</li>
<li>Application for Android</li>
</ul>
<p>&nbsp;</p>
<p><strong>Short brief:</strong></p>
<p>A Vibrant Concepts is trying to setup an online platform which will work in the education sector. The main purpose of the company is to create a system in which people can learn new skills with the ease of their home. This project will comprise of&nbsp;mobile application and a website where there are three types of user mainly Admin, Teacher and Student. There will be an admin panel to manage all the features and the module that are there in the project. The system is developed with a user-friendly and attractive GUI. It is a platform to interact teacher, student and parents. Users have to first login into the system to view the classes, courses and to attend online classes or to view course progress. They can track their progress they have made on the project basis and also they can make the payment and attend to video lectures through the portal.<strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Development Modules</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Features :</strong></p>
<ul>
<li><strong>Course and its management</strong></li>
<li><strong>Syllabus of the course and its management</strong></li>
<li><strong>Teacher, his profile and its management</strong></li>
<li><strong>Course Preview for users</strong></li>
<li><strong>Social Login for easy login</strong></li>
<li><strong>Evaluation system (optional)</strong></li>
<li><strong>Course Search and Filter</strong></li>
<li><strong>Instructors Earnings and Report&nbsp;</strong></li>
<li><strong>Wishlist</strong></li>
<li><strong>Attachments</strong></li>
<li><strong>Assignments</strong></li>
<li><strong>Modern Lecture Page</strong></li>
<li><strong>Course reviews and rating system</strong></li>
<li><strong>Blog</strong></li>
<li><strong>Withdraw Earning Module</strong></li>
<li><strong>Discussions (Optional)</strong></li>
<li><strong>Media Manager (Optional)</strong></li>
<li><strong>Course Progress (Optional)</strong></li>
<li><strong>Built-in Accounting System&nbsp;</strong></li>
<li><strong>Coming Soon Courses System and its management</strong></li>
</ul>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>User Android Application :</strong></p>
<p><strong>&nbsp;</strong></p>
<p>On the application user has to register on this application on the time of registration user needs to give personal details after registration user will be login with username and password. On the user application he will be able to do view the list of categories that are available for the users. They can view the available courses on the category basis and also will have a search system to find the course. They can select any course and buy it to learn. The payment gateway will also be integrated. User can watch the video inside the app only.<strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Project Execution:</strong></p>
<p>&nbsp;</p>
<p>As we discussed, we will start work on your Product. Especially for this project, there will be 1 Project Manager, 1 Web Developer,&nbsp;As Discussed, we are&nbsp;going for React Development then 1 React Native Developer otherwise 1 Android Developer and 1 iOS developer. The product development will be held on premises of Backstage Supporters Pvt. Ltd &amp; we will be in contact with you via Phone, Skype, Email or any other communication media as you prefer on a regular basis. You need to provide all required information before starting the work. To complete this Project we will take the next 60 working days (i.e. Monday to Friday) from the approval date.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Cost</strong></p>
<p><strong>&nbsp;</strong></p>
<p>The table below details the costs associated with this project. Invoices will be sent to clients on the dates indicated below, are payable via credit card or wire transfer, and are due on a net-30 basis.</p>
<p><strong>&nbsp;</strong></p>
<table width="592">
<tbody>
<tr>
<td width="354">
<p><strong>Name</strong></p>
</td>
<td width="238">
<p><strong>Price</strong></p>
</td>
</tr>
<tr>
<td width="354">
<p>Admin Panel</p>
</td>
<td width="238">
<p>Rs. 70,000/-</p>
</td>
</tr>
<tr>
<td width="354">
<p>Website</p>
</td>
<td width="238">
<p>Rs. 40,000/-</p>
</td>
</tr>
<tr>
<td width="354">
<p>Application</p>
</td>
<td width="238">
<p>Rs. 40,000/-</p>
</td>
</tr>
<tr>
<td width="354">
<p>Logo (Optional)</p>
</td>
<td width="238">
<p>Rs 1000 for basic design</p>
</td>
</tr>
<tr>
<td width="354">
<p><strong>Total</strong></p>
</td>
<td width="238">
<p>Rs. 1,51,000/- + 18% GST</p>
</td>
</tr>
</tbody>
</table>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Terms of Agreement</strong></p>
<p>This Agreement shall commence on the Effective Date and, unless otherwise terminated according to the provisions of Sub Sections in this Agreement, will continue until the Software has been satisfactorily completed and handed over to the Company and also the Contractor has been paid in full according to the Payment Terms in Proposal.&nbsp;Here Backstage Supporters Private Limited is refered to as Contractor and Vibrant Concepts As the Company.</p>
<p><strong>Authorization</strong></p>
<p>Backstage Supporters Pvt. Ltd. is engaging with Verifacts, as an independent contractor for the specific project outlined below:</p>
<p><strong>Vibrant Concepts</strong></p>
<p><strong>Backstage Supporters Pvt. Ltd. during Execution</strong></p>
<p>Vibrant Concepts, may be charged additional fees if it decides to make changes to the agreed upon project scope and objectives.</p>
<p><strong>Initial Payment &amp; Refund Policy</strong></p>
<p><strong>This agreement begins with an initial payment indicated in the pricing table. If <strong>Vibrant Concepts</strong> halts work and applies for a refund within 4 days, work completed shall be billed at the hourly rate of (INR146/- per Hour), and deducted from the initial payment, the balance of which shall be returned to Vibrant Concepts. If at the time of the request for refund, work has been completed beyond the amount covered by the initial payment, <strong>Vibrant Concepts</strong> shall be liable to pay for all work completed at the hourly rate stated above.</strong></p>
<p><strong>No portion of this initial payment will be refunded unless requested within 4 days of signing this contract.</strong></p>
<p>&nbsp;</p>
<p><strong>Types of Termination</strong>. This Agreement may be terminated by;</p>
<p>&nbsp;</p>
<ul>
<li>Either Party on provision of Twenty (10) days written notice to the other Party.</li>
<li>Either Party for&nbsp;quality breach of any provision of this Agreement by the other Party, if the other Party's quality&nbsp;breach is not cured within Fifteen (45) days of receipt of written notice thereof.</li>
<li>The Company at any given time without prior notice, if the Contractor is convicted of any crime of offense, fails or refuses to comply with reasonable directives of the Company, or is guilty of serious misconduct in connection with performance under this Agreement.&nbsp;</li>
</ul>
<p><strong>Responsibilities after Termination.</strong>&nbsp;</p>
<ul>
<li>Following the termination of this Agreement for any reasons, the Company shall promptly pay the Contractor according to the Project Time-line and Payment Terms agreed upon for work that is rendered before the effective date of the termination ("Termination Date"). The Contractor acknowledges and agrees that all intellectual property developed pursuant to this Agreement before the Termination Date shall be delivered to the Company within one (2) day from the Termination Date.</li>
</ul>
<p>&nbsp;<strong>COMPENSATIONS</strong></p>
<ul>
<li>The total compensation of the designing and development of the Software shall be set forth hereto. These payments shall be made in instalments according to the schedule set forth hereto.</li>
</ul>
<p>&nbsp;<strong>RESPONSIBILITIES</strong></p>
<p>&nbsp;Backstage Supporters agrees to do each of the following:</p>
<p>&nbsp;</p>
<ul>
<li>Create the Application System as detailed in Exhibit A to this Agreement, and extend its best efforts to ensure that the design and functionality of the Application System meets the Company&rsquo;s specifications.&nbsp;</li>
</ul>
<ul>
<li>Devote as much productive time, energy, and ability to the performance of its duties hereunder as may be necessary to provide the required Services in a timely and productive manner and to the timeframe specified.&nbsp;</li>
</ul>
<ul>
<li>Perform the Services in a workmanlike manner and with professional diligence and skill, as a fully-trained, skilled, competent, and experienced personnel.&nbsp;</li>
</ul>
<ul>
<li>On completion of the Application System, assist the Company in installation of the Application System to its final location, which assistance will include helping the Company with its upload of the finished files to the Company&rsquo;s selected Web-hosting company and submitting for approval on the Apple/Android Store.</li>
<li>Provide all files and codes to the Company.</li>
<li>Provide Services and an Application System that are satisfactory and acceptable to the Company and substantially free of defects.</li>
<li>Communicate with the Company regarding progress it has made with respect to the milestones listed in performing the Services upon an agreeable time each week.</li>
</ul>
<p>The&nbsp;Vibrant Concept agrees to do each of the following:&nbsp;</p>
<ul>
<li>Provide all assistance and cooperation to the Contractor in order to complete the Application System timely and efficiently.</li>
<li>Provide initial information, and supply contents for the Application System.</li>
</ul>
<p>&nbsp;<strong>WARRANTY PERIOD</strong></p>
<p>Backstage Supporters Pvt Ltd&nbsp;agrees to provide continued warranty support for the Software&rsquo;s for 270 days (9months) after the websites, iOS and Andriod applications has been successfully uploaded and Go-Live ("Warranty Period). The Warranty Period shall refer to any bugs or issues relating to the features specified. This warranty period will be provided to the Company at no additional cost.</p>
<p>&nbsp;<strong>CONFIDENTIAL INFORMATION</strong></p>
<p>All information relating to Company that is known to be confidential or proprietary, or which is clearly marked as such, will be held in Strictest confidence by Backstage Supporters and shall not be disclosed or used by Contractor except to the extent that such disclosure or use is reasonably necessary to the performance of Contractor&rsquo;s Work.</p>
<ul>
<li>All information relating to Contractor that is known to be confidential or proprietary, or which is clearly marked as such, will be held in strictest confidence by Company and shall not be disclosed or used by Company except to the extent that such disclosure or use is reasonably necessary to the performance of the Company&rsquo;s duties and obligations under this Agreement.</li>
</ul>
<p>&nbsp;</p>
<p>"Confidential Information" means any of the propriety information, technical data, trade secrets, or know-how, including, but not limited to, business plans, research, product plans, products, services, Company lists, markets, software, developments, inventions, processes, formulas, technology, designs, drawings, engineering, hardware configuration information, marketing, finances, or other business information disclosed to either Party either directly or indirectly.&nbsp;</p>
<p>The obligations of confidentiality will extend for a period of <u>12 months</u> after the termination of this Agreement, but will not apply with respect to information that is independently developed by the parties, lawfully becomes a part of the public domain, or of which the parties gained knowledge or possession free of any confidentiality obligation.<strong>&nbsp;</strong></p>
<p><strong>OBLIGATIONS</strong></p>
<p>The Services performed by the Contractor shall be in accordance with and shall not violate any applicable laws, rules, or regulations and the Contractor shall obtain all or any permits or permission required (if applicable) in order to comply with such laws, rules, or regulations. If the Contractor intends to integrate or incorporate any works that it had previously created into any work product to be created in furtherance of its performance of the Services, the Contractor must obtain the Company's prior written approval of such integration or incorporation. The Contractor hereby warrants to the Company that, to the best of its knowledge, is not currently obliged under any existing contract or other duty that conflicts with or is inconsistent with this Agreements.&nbsp;</p>
<p>The Contractor warrants to ensure and represents that the Application System, when delivered or accessed by the Company, will be free from material defects, and from viruses, logic locks, and other disabling devices or codes, and in particular will not contain any virus, Trojan horse, worm, drop-dead devices, trap doors, time bombs, or other software routines or other hardware component that could permit unauthorized access, disable, erase, or otherwise harm the Application System or any software, hardware, or data, cause the Application System or any software or hardware to perform any functions other than those specified in this Agreement, halt, disrupt, or degrade the operation of the Application System or any software or hardware, or perform any other such actions.&nbsp;</p>
<p>In any event that such discovery is discovered, and in particular that any devices, codes or any other form of access are used to gain unauthorized access into the system either to disable, disrupt, adjust and/or to make unauthorized transactions, legal actions will be made against the Contractor and all liabilities shall be bare by the Contractor.</p>
<p>&nbsp;<strong>NON-INFRINGEMENT OF INTELLECTUAL PROPERTY</strong>&nbsp;</p>
<ul>
<li>No Intellectual Property Infringement by Contractor. The Contractor hereby represents and warrants that the use and proposed use of the Software by the Company or any third party does not and shall not infringe, and the Contractor has not received any notice, complaint, threat, or claim alleging infringement of, any trademark, copyright, patent, trade secrets, industrial design, or other rights of any third party in the Software, and the use of the</li>
</ul>
<p>&nbsp;</p>
<p>Software will not include any activity that may constitute &ldquo;passing off.&rdquo; To the extent the Software infringes on the rights of any such third party, the Contractor shall obtain a license or consent from such third party permitting the use of the rights and the cost shall be fully bare by the Contractor. In an event that such infringement by the Contractor and/or Subcontractor occurs, and that result in any claims or law suit that is being made against the Company for any form of compensations, the Contractor will and shall bare and defend the Company and its officers, members, managers, employees, agents, contractors, sublicenses, affiliates, subsidiaries, successors against any and all damages and liabilities at all times (including attorney's fees and court cost)</p>
<ul>
<li>No Intellectual Property Infringement by Company. The Company represents to the Contractor and unconditionally guarantees that any elements of text, graphics, photos, designs, trademarks, or other artwork furnished to the Contractor for inclusion in the Website are owned by the Company, or that the Company has permission from the rightful owner to use each of these elements, and will hold harmless, protect, indemnify, and defend the Contractor and its Subcontractors from any liability (including attorneys&rsquo; fees and court costs), including any claim or suit, threatened or actual, arising from the use of such elements furnished by the Company.&nbsp;</li>
</ul>
<ul>
<li>Continuing Ownership of Existing Trademarks. The Contractor and Subcontractor recognizes the Company&rsquo;s right, title, and interest in and to all service marks, trademarks, and trade names used by the Company and agrees not to engage in any activities or commit any acts, directly or indirectly, that may contest, dispute, or otherwise impair the Company&rsquo;s right, title, and interest therein, nor shall the Contractor and Subcontractor cause diminishment of value of said trademarks or trade names through any act or representation. The Contractor and Subcontractor shall not apply for, acquire, or claim any right, title, or interest in or to any such service marks, trademarks, or trade names, or others that may be confusingly similar to any of them, through advertising or otherwise. Effective as of the termination of this Agreement, the Contractor and Subcontractor shall cease to use all of the Company&rsquo;s trademarks, marks, and trade names.&nbsp;</li>
</ul>
<p><strong>OWNERSHIP OF INTELLECTUAL PROPERTY</strong></p>
<p>&nbsp;</p>
<p>To the extent and/or upon completion of the Service that the Contractor has received payment of compensation as provided in this agreement, The Contractor expressly acknowledges and agrees that any and all proprietary code prepared by the Contractor or any of it's Subcontractor under this Agreement shall have the ownership assigned and handed over to the Company. These items shall include, but shall not be limited to, any and all deliverables resulting from the Contractor&rsquo;s Services or contemplated by this Agreement, all tangible results and proceeds of the Services, coding, works in progress, records, diagrams, notes, drawings, specifications, schematics, documents, designs, improvements, inventions, discoveries, developments, trademarks, trade secrets, Company lists, databases, software, programs, middleware, applications, and solutions conceived, made, or discovered by the Contractor, solely or in collaboration with others, during the Term of this Agreement relating in any manner to the Contractor&rsquo;s Services.</p>
<p><strong>RELATION OF PARTIES</strong>&nbsp;</p>
<p>The performance by Contractor of its duties and obligations under this Agreement will be that of an independent contractor, and nothing in this agreement will create or imply an agency relationship between Contractor and Company.</p>
<p><strong>ARBITRATION AND MEDIATION</strong></p>
<p>Except as described, if any dispute arises under the terms of this Agreement, the parties agree to select a mutually agreeable neutral third party to help them mediate it. If the mediation is unsuccessful, the parties agree that the dispute shall be decided by binding arbitration under the rules issued by the Indian Government. The decision of the arbitrator shall be final. Costs and fees (other than attorneys&rsquo; fees associated with the mediation or arbitration shall be shared equally by the parties. Each party shall be responsible for its own attorneys&rsquo; fees associated with arbitration.<strong>&nbsp;</strong></p>
<p><strong>FORCE MAJEURE</strong></p>
<p>A Party shall be not be considered in breach of or in default under this Agreement on account of, and shall not be liable to the other Party for, any delay or failure to perform its obligations hereunder by reason of fire, earthquake, flood, explosion, strike, riot, war, terrorism, or similar event beyond that Party&rsquo;s reasonable control (each a &ldquo;Force Majeure Event&rdquo;); provided, however, if a Force Majeure Event occurs, the affected Party shall, as soon as practicable:</p>
<ul>
<li>Notify the other Party of the Force Majeure Event and its impact on performance under this Agreement; and&nbsp;</li>
</ul>
<ul>
<li>Use reasonable efforts to resolve any issues resulting from the Force Majeure Event and perform its obligations hereunder.</li>
</ul>
<p>&nbsp;<strong>NO IMPLIED WAIVER</strong>&nbsp;</p>
<p>The failure of either Party to insist on strict performance of any covenant or obligation under this Agreement, regardless of the length of time for which such failure continues, shall not be deemed a waiver of such Party's right to demand strict compliance in the future. No consent or waiver, express or implied, to or of any breach or default in the performance of any obligation under this Agreement shall constitute a consent or waiver to or of any other breach or default in the performance of the same or any other obligation.</p>
<p><strong>SEVERABILITY</strong></p>
<p>Whenever possible, each provision of this Agreement will be interpreted in such manner as to be effective and valid under applicable law, but if any provision of this Agreement is held to be invalid, illegal, or unenforceable in any respect under any applicable law or rule in any jurisdiction, such invalidity, illegality, or unenforceability will not affect any other provision or any other jurisdiction, but this Agreement will be reformed, construed, and enforced in such jurisdiction as if such invalid, illegal, or unenforceable provisions had never been contained herein.</p>
<p>&nbsp;<strong>AMENDMENTS</strong></p>
<p>This Agreement shall be construed pursuant to the laws and declared to be legally binding in the Indian Judiciary System. This Agreement may not be modified or amended except by written notice, which must be signed by authorized representatives of each of the parties. A party&rsquo;s failure to exercise, or delay in exercising any rights hereunder will not be deemed to be a waiver of such right (we can take away this part as included in Section 13). If any provision of this Agreement is held invalid or otherwise unenforceable, the enforceability of the remaining provisions of this Agreement will not be impaired thereby (we can take away this part as included in Section 14). IN WITNESS WHEREOF, the parties have executed this Agreement effective the date first stated above.</p>
<p>&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Backstage Supporters Pvt. Ltd. </strong></p>
<p><strong>&nbsp;</strong></p>
<p>Signature and stamp:</p>
<p>&nbsp;</p>
<p>Client Full Name: <strong>Vibrant Concepts LLP</strong></p>
<p>Designation:</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Signature and Company Stamp</p>
<p>&nbsp;</p>
<p><strong>&nbsp;</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
