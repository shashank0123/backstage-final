@extends('admin.layouts.admin')
@section('content')
<div class='col-md-12' style=''><Form action='/admin/project_proposal/add' method='post'>@csrf<div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Project Id</label>
                <input class='form-control btn-square' type='text' name='project_id' placeholder='Enter Project Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Client Id</label>
                <input class='form-control btn-square' type='text' name='client_id' placeholder='Enter Client Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Template Id</label>
                <input class='form-control btn-square' type='text' name='template_id' placeholder='Enter Template Id'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Sent On</label>
                <input class='form-control btn-square' type='text' name='sent_on' placeholder='Enter Sent On'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Version</label>
                <input class='form-control btn-square' type='text' name='version' placeholder='Enter Version'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div><div class='form-group ui-draggable-handle' style='position: static;'>
                <label for='input-text-1'>Approval Status</label>
                <input class='form-control btn-square' type='text' name='approval_status' placeholder='Enter Approval Status'>
                <p style='display:none' class='help-block'>Example block-level help text here.</p>
            </div></div>
    <div class='col-md-12' style=''>
        <div class='form-group ui-draggable-handle' style='position: static;'>

            <button class='btn btn-primary' type='submit' data-original-title='Save And Return' title=''>Save</button>
            <a href='/admin/project_proposal' class='btn btn-secondary' data-original-title='Cancel And Go Back' title=''>Cancel</a>
            <p style='display:none' class='help-block'>Press Submit to save</p>
        </div>
    </div>
    </Form>
@endsection