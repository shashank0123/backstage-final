<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/dashboard', 'Admin\DashboardController@getDashboard');
Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Admin\AdminController@index')->name('admin.home');
Route::get('/create-module', 'Admin\ModuleController@createView')->middleware('auth:admin')->name('admin.module.create');

Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/project_language', 'Admin\ProjectLanguageController@index');
    Route::get('project_language/add', 'Admin\ProjectLanguageController@add');
    Route::post('project_language/add', 'Admin\ProjectLanguageController@store');
    Route::get('project_language/edit/{id}', 'Admin\ProjectLanguageController@edit');
    Route::post('project_language/edit/{id}', 'Admin\ProjectLanguageController@update');
    Route::post('project_language/delete/{id}', 'Admin\ProjectLanguageController@delete');
    Route::get('/project_module', 'Admin\ProjectModuleController@index');
    Route::get('project_module/add', 'Admin\ProjectModuleController@add');
    Route::post('project_module/add', 'Admin\ProjectModuleController@store');
    Route::get('project_module/edit/{id}', 'Admin\ProjectModuleController@edit');
    Route::post('project_module/edit/{id}', 'Admin\ProjectModuleController@update');
    Route::post('project_module/delete/{id}', 'Admin\ProjectModuleController@delete');
    Route::get('/project', 'Admin\ProjectController@index');
    Route::get('project/add', 'Admin\ProjectController@add');
    Route::post('project/add', 'Admin\ProjectController@store');
    Route::get('project/edit/{id}', 'Admin\ProjectController@edit');
    Route::post('project/edit/{id}', 'Admin\ProjectController@update');
    Route::post('project/delete/{id}', 'Admin\ProjectController@delete');
    Route::get('/client', 'Admin\ClientController@index');
    Route::get('client/add', 'Admin\ClientController@add');
    Route::post('client/add', 'Admin\ClientController@store');
    Route::get('client/edit/{id}', 'Admin\ClientController@edit');
    Route::post('client/edit/{id}', 'Admin\ClientController@update');
    Route::post('client/delete/{id}', 'Admin\ClientController@delete');
    Route::get('/theme', 'Admin\ThemeController@index');
    Route::get('theme/add', 'Admin\ThemeController@add');
    Route::post('theme/add', 'Admin\ThemeController@store');
    Route::get('theme/edit/{id}', 'Admin\ThemeController@edit');
    Route::post('theme/edit/{id}', 'Admin\ThemeController@update');
    Route::post('theme/delete/{id}', 'Admin\ThemeController@delete');
    Route::get('/themeelement', 'Admin\ThemeElementController@index');
    Route::get('themeelement/add', 'Admin\ThemeElementController@add');
    Route::post('themeelement/add', 'Admin\ThemeElementController@store');
    Route::get('themeelement/edit/{id}', 'Admin\ThemeElementController@edit');
    Route::post('themeelement/edit/{id}', 'Admin\ThemeElementController@update');
    Route::post('themeelement/delete/{id}', 'Admin\ThemeElementController@delete');
    Route::get('/themesettings', 'Admin\ThemeSettingsController@index');
    Route::get('themesettings/add', 'Admin\ThemeSettingsController@add');
    Route::post('themesettings/add', 'Admin\ThemeSettingsController@store');
    Route::get('themesettings/edit/{id}', 'Admin\ThemeSettingsController@edit');
    Route::post('themesettings/edit/{id}', 'Admin\ThemeSettingsController@update');
    Route::post('themesettings/delete/{id}', 'Admin\ThemeSettingsController@delete');
    Route::get('/projecttheme', 'Admin\ProjectThemeController@index');
    Route::get('projecttheme/add', 'Admin\ProjectThemeController@add');
    Route::post('projecttheme/add', 'Admin\ProjectThemeController@store');
    Route::get('projecttheme/edit/{id}', 'Admin\ProjectThemeController@edit');
    Route::post('projecttheme/edit/{id}', 'Admin\ProjectThemeController@update');
    Route::post('projecttheme/delete/{id}', 'Admin\ProjectThemeController@delete');
    Route::get('/slider', 'Admin\SliderController@index');
    Route::get('slider/add', 'Admin\SliderController@add');
    Route::post('slider/add', 'Admin\SliderController@store');
    Route::get('slider/edit/{id}', 'Admin\SliderController@edit');
    Route::post('slider/edit/{id}', 'Admin\SliderController@update');
    Route::post('slider/delete/{id}', 'Admin\SliderController@delete');
    Route::get('/project_language', 'Admin\ProjectLanguageController@index');
    Route::get('project_language/add', 'Admin\ProjectLanguageController@add');
    Route::post('project_language/add', 'Admin\ProjectLanguageController@store');
    Route::get('project_language/edit/{id}', 'Admin\ProjectLanguageController@edit');
    Route::post('project_language/edit/{id}', 'Admin\ProjectLanguageController@update');
    Route::post('project_language/delete/{id}', 'Admin\ProjectLanguageController@delete');
    Route::get('/project_dependency', 'Admin\ProjectDependencyController@index');
    Route::get('project_dependency/add', 'Admin\ProjectDependencyController@add');
    Route::post('project_dependency/add', 'Admin\ProjectDependencyController@store');
    Route::get('project_dependency/edit/{id}', 'Admin\ProjectDependencyController@edit');
    Route::post('project_dependency/edit/{id}', 'Admin\ProjectDependencyController@update');
    Route::post('project_dependency/delete/{id}', 'Admin\ProjectDependencyController@delete');
    Route::get('/project_proposal', 'Admin\ProjectProposalController@index');
    Route::get('project_proposal/add', 'Admin\ProjectProposalController@add');
    Route::post('project_proposal/add', 'Admin\ProjectProposalController@store');
    Route::get('project_proposal/edit/{id}', 'Admin\ProjectProposalController@edit');
    Route::post('project_proposal/edit/{id}', 'Admin\ProjectProposalController@update');
    Route::post('project_proposal/delete/{id}', 'Admin\ProjectProposalController@delete');
    Route::get('/proposal_template', 'Admin\ProposalTemplateController@index');
    Route::get('proposal_template/add', 'Admin\ProposalTemplateController@add');
    Route::post('proposal_template/add', 'Admin\ProposalTemplateController@store');
    Route::get('proposal_template/edit/{id}', 'Admin\ProposalTemplateController@edit');
    Route::post('proposal_template/edit/{id}', 'Admin\ProposalTemplateController@update');
    Route::post('proposal_template/delete/{id}', 'Admin\ProposalTemplateController@delete');
    Route::get('/lead', 'Admin\LeadController@index');
    Route::get('lead/add', 'Admin\LeadController@add');
    Route::post('lead/add', 'Admin\LeadController@store');
    Route::get('lead/edit/{id}', 'Admin\LeadController@edit');
    Route::post('lead/edit/{id}', 'Admin\LeadController@update');
    Route::post('lead/delete/{id}', 'Admin\LeadController@delete');
    Route::get('/invoice', 'Admin\InvoiceController@index');
    Route::get('invoice/add', 'Admin\InvoiceController@add');
    Route::post('invoice/add', 'Admin\InvoiceController@store');
    Route::get('invoice/edit/{id}', 'Admin\InvoiceController@edit');
    Route::post('invoice/edit/{id}', 'Admin\InvoiceController@update');
    Route::post('invoice/delete/{id}', 'Admin\InvoiceController@delete');
    Route::get('/project_module_upload', 'Admin\ProjectModuleUploadController@index');
    Route::get('project_module_upload/add', 'Admin\ProjectModuleUploadController@add');
    Route::post('project_module_upload/add', 'Admin\ProjectModuleUploadController@store');
    Route::get('project_module_upload/edit/{id}', 'Admin\ProjectModuleUploadController@edit');
    Route::post('project_module_upload/edit/{id}', 'Admin\ProjectModuleUploadController@update');
    Route::get('project_module_upload/delete/{id}', 'Admin\ProjectModuleUploadController@delete');
    Route::get('/teacher', 'Admin\TeacherController@index');
    Route::get('teacher/add', 'Admin\TeacherController@add');
    Route::post('teacher/add', 'Admin\TeacherController@store');
    Route::get('teacher/edit/{id}', 'Admin\TeacherController@edit');
    Route::post('teacher/edit/{id}', 'Admin\TeacherController@update');
    Route::get('teacher/delete/{id}', 'Admin\TeacherController@delete');
    Route::get('/studentclass', 'Admin\StudentClassController@index');
    Route::get('studentclass/add', 'Admin\StudentClassController@add');
    Route::post('studentclass/add', 'Admin\StudentClassController@store');
    Route::get('studentclass/edit/{id}', 'Admin\StudentClassController@edit');
    Route::post('studentclass/edit/{id}', 'Admin\StudentClassController@update');
    Route::get('studentclass/delete/{id}', 'Admin\StudentClassController@delete');
    Route::get('/queryform', 'Admin\QueryFormController@index');
    Route::get('queryform/add', 'Admin\QueryFormController@add');
    Route::post('queryform/add', 'Admin\QueryFormController@store');
    Route::get('queryform/edit/{id}', 'Admin\QueryFormController@edit');
    Route::post('queryform/edit/{id}', 'Admin\QueryFormController@update');
    Route::get('queryform/delete/{id}', 'Admin\QueryFormController@delete');
    Route::get('/slider', 'Admin\SliderController@index');
    Route::get('slider/add', 'Admin\SliderController@add');
    Route::post('slider/add', 'Admin\SliderController@store');
    Route::get('slider/edit/{id}', 'Admin\SliderController@edit');
    Route::post('slider/edit/{id}', 'Admin\SliderController@update');
    Route::get('slider/delete/{id}', 'Admin\SliderController@delete');
    Route::get('/subject', 'Admin\SubjectController@index');
    Route::get('subject/add', 'Admin\SubjectController@add');
    Route::post('subject/add', 'Admin\SubjectController@store');
    Route::get('subject/edit/{id}', 'Admin\SubjectController@edit');
    Route::post('subject/edit/{id}', 'Admin\SubjectController@update');
    Route::get('subject/delete/{id}', 'Admin\SubjectController@delete');
    Route::get('/teachersubject', 'Admin\TeacherSubjectController@index');
    Route::get('teachersubject/add', 'Admin\TeacherSubjectController@add');
    Route::post('teachersubject/add', 'Admin\TeacherSubjectController@store');
    Route::get('teachersubject/edit/{id}', 'Admin\TeacherSubjectController@edit');
    Route::post('teachersubject/edit/{id}', 'Admin\TeacherSubjectController@update');
    Route::get('teachersubject/delete/{id}', 'Admin\TeacherSubjectController@delete');
    Route::get('/topic', 'Admin\TopicController@index');
    Route::get('topic/add', 'Admin\TopicController@add');
    Route::post('topic/add', 'Admin\TopicController@store');
    Route::get('topic/edit/{id}', 'Admin\TopicController@edit');
    Route::post('topic/edit/{id}', 'Admin\TopicController@update');
    Route::get('topic/delete/{id}', 'Admin\TopicController@delete');
    Route::get('/sms_notification','Admin\SmsNotificationController@index');
    Route::get('sms_notification/add','Admin\SmsNotificationController@add');
    Route::post('sms_notification/add','Admin\SmsNotificationController@store');
    Route::get('sms_notification/edit/{id}','Admin\SmsNotificationController@edit');
    Route::post('sms_notification/edit/{id}','Admin\SmsNotificationController@update');
    Route::get('sms_notification/delete/{id}','Admin\SmsNotificationController@delete');
//{Route to be added here from automation}
});
