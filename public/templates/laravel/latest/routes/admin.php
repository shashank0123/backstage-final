<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/login', 'Admin\Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/login', 'Admin\Auth\AdminLoginController@login')->name('admin.login.submit');
Route::get('/home', 'Admin\AdminController@index')->name('admin.home');


Route::group(['middleware' => ['auth:admin']], function () {
    Route::get('/dashboard', 'Admin\DashboardController@getDashboard');
    Route::get('/teacher', 'Admin\TeacherController@index');
    Route::get('teacher/add', 'Admin\TeacherController@add');
    Route::post('teacher/add', 'Admin\TeacherController@store');
    Route::get('teacher/edit/{id}', 'Admin\TeacherController@edit');
    Route::post('teacher/edit/{id}', 'Admin\TeacherController@update');
    Route::post('teacher/delete/{id}', 'Admin\TeacherController@delete');
    Route::get('/teachersubject', 'Admin\TeacherSubjectController@index');
    Route::get('teachersubject/add', 'Admin\TeacherSubjectController@add');
    Route::post('teachersubject/add', 'Admin\TeacherSubjectController@store');
    Route::get('teachersubject/edit/{id}', 'Admin\TeacherSubjectController@edit');
    Route::post('teachersubject/edit/{id}', 'Admin\TeacherSubjectController@update');
    Route::post('teachersubject/delete/{id}', 'Admin\TeacherSubjectController@delete');
    Route::get('/class', 'Admin\ClassController@index');
    Route::get('class/add', 'Admin\ClassController@add');
    Route::post('class/add', 'Admin\ClassController@store');
    Route::get('class/edit/{id}', 'Admin\ClassController@edit');
    Route::post('class/edit/{id}', 'Admin\ClassController@update');
    Route::post('class/delete/{id}', 'Admin\ClassController@delete');
    Route::get('/subject', 'Admin\SubjectController@index');
    Route::get('subject/add', 'Admin\SubjectController@add');
    Route::post('subject/add', 'Admin\SubjectController@store');
    Route::get('subject/edit/{id}', 'Admin\SubjectController@edit');
    Route::post('subject/edit/{id}', 'Admin\SubjectController@update');
    Route::post('subject/delete/{id}', 'Admin\SubjectController@delete');
    Route::get('/topic', 'Admin\TopicController@index');
    Route::get('topic/add', 'Admin\TopicController@add');
    Route::post('topic/add', 'Admin\TopicController@store');
    Route::get('topic/edit/{id}', 'Admin\TopicController@edit');
    Route::post('topic/edit/{id}', 'Admin\TopicController@update');
    Route::post('topic/delete/{id}', 'Admin\TopicController@delete');
    Route::get('/queryform', 'Admin\QueryFormController@index');
    Route::get('queryform/add', 'Admin\QueryFormController@add');
    Route::post('queryform/add', 'Admin\QueryFormController@store');
    Route::get('queryform/edit/{id}', 'Admin\QueryFormController@edit');
    Route::post('queryform/edit/{id}', 'Admin\QueryFormController@update');
    Route::post('queryform/delete/{id}', 'Admin\QueryFormController@delete');
    Route::get('/slider', 'Admin\SliderController@index');
    Route::get('slider/add', 'Admin\SliderController@add');
    Route::post('slider/add', 'Admin\SliderController@store');
    Route::get('slider/edit/{id}', 'Admin\SliderController@edit');
    Route::post('slider/edit/{id}', 'Admin\SliderController@update');
    Route::post('slider/delete/{id}', 'Admin\SliderController@delete');
    //{Route to be added here from automation}
});
