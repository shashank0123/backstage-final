<nav>
            <div class="main-navbar">
              <div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
              <div id="mainnav">
                <ul class="nav-menu custom-scrollbar">
                  <li class="back-btn">
                    <div class="mobile-back text-right"><span>Back</span><i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title link-nav" href="/admin/dashboard"><i data-feather="anchor"></i><span>Dashboard</span></a></li>

                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Class</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/class/add">Add Class</a></li>
                      <li><a href="/admin/class">Class List</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Subject</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/subject/add">Add Subject</a></li>
                      <li><a href="/admin/subject">Subject List</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Topic</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/topic/add">Add Topic</a></li>
                      <li><a href="/admin/topic">Topic List</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Slider</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/slider/add">Add Slider</a></li>
                      <li><a href="/admin/slider">Slider List</a></li>
                    </ul>
                  </li>
                  <li class="dropdown"><a class="nav-link menu-title" href="#"><i data-feather="home"></i><span>Query</span></a>
                    <ul class="nav-submenu menu-content">
                      <li><a href="/admin/query_form/add">Add Query</a></li>
                      <li><a href="/admin/query_form">Query List</a></li>
                    </ul>
                  </li>

                </ul>
              </div>
              <div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
            </div>
          </nav>
